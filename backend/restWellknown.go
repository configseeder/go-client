package backend

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
	"io"
	"net/http"
	"strings"
)

type WellKnownBackend interface {
	// GetWellKnownFromConfigSeeder calls the ConfigSeeders endpoint to retrieve well known information like the ConfigSeeders public url
	//
	GetWellKnownFromConfigSeeder(clientConfiguration *config.ClientConfiguration) (*model.WellKnown, error)
}

type WellKnownBackendImplementation struct {
	info util.UserAgentInfo
}

func InitWellKnown(info util.UserAgentInfo) WellKnownBackend {
	var backend WellKnownBackend = &WellKnownBackendImplementation{
		info: info,
	}

	return backend
}

func (b *WellKnownBackendImplementation) GetWellKnownFromConfigSeeder(clientConfiguration *config.ClientConfiguration) (*model.WellKnown, error) {
	wellknownUrl := b.getWellKnownUrl(*clientConfiguration.ServerUrl)

	httpRequest, err := b.createRequest(wellknownUrl, *clientConfiguration.ApiKey)
	if err != nil {
		return nil, err
	}

	client := getHttpClient(clientConfiguration)

	response, err := client.Do(httpRequest)
	if response != nil {
		defer func(Body io.ReadCloser) {
		_:
			Body.Close()
		}(response.Body)
	}

	err = handleError(err, wellknownUrl)
	if err != nil {
		return nil, err
	}

	return b.unpackFromResponse(response)

}

func (b *WellKnownBackendImplementation) getWellKnownUrl(url string) string {
	propertiesUrl := url
	if !strings.HasSuffix(propertiesUrl, "/") {
		propertiesUrl += "/"
	}
	propertiesUrl += ".well-known/config-seeder/client/global-config"
	return propertiesUrl
}

// createRequest creates the request object which is sent to ConfigSeeder for requesting the well known data.
func (b *WellKnownBackendImplementation) createRequest(wellknownUrl string, apiKey string) (*http.Request, error) {
	httpRequest, err := http.NewRequest("GET", wellknownUrl, nil)
	if err != nil {
		message := fmt.Sprintf("Unable to create GET request for %s: %s", wellknownUrl, err)
		return nil, errors.New(message)
	}

	httpRequest.Header.Add("Accept", "application/json")
	httpRequest.Header.Add("Authorization", "Bearer "+apiKey)

	util.AddCustomHeadersToRequest(httpRequest, &b.info)

	httpRequest.Close = true

	return httpRequest, nil
}

// unpackFromResponse gets the well known information out of the http.Response.
func (b *WellKnownBackendImplementation) unpackFromResponse(response *http.Response) (*model.WellKnown, error) {
	responseJson, err := io.ReadAll(response.Body)
	if err != nil {
		message := fmt.Sprintf("Unable to read body from response: %s", err)
		return nil, errors.New(message)
	}

	if http.StatusOK != response.StatusCode {
		message := fmt.Sprintf("ConfigServer returned unexpectet status %s, body: %s", response.Status, responseJson)
		return nil, errors.New(message)
	}

	wellKnown := &model.WellKnown{}
	err = json.Unmarshal(responseJson, wellKnown)

	if err != nil {
		message := fmt.Sprintf("Unable to deserialise json to WellKnown information: (%v): %s", response, err)
		return nil, errors.New(message)
	}

	return wellKnown, nil
}
