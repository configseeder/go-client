package backend

import (
	"bytes"
	"io"
	"net/http"
	"testing"
)

func TestWellKnownBackendImplementation_getWellKnownUrlWithSlash(t *testing.T) {
	backend := WellKnownBackendImplementation{}
	actual := backend.getWellKnownUrl("prefix/")

	expectedValue := "prefix/.well-known/config-seeder/client/global-config"
	if actual != expectedValue {
		t.Errorf("expected url to be %s but was %s", expectedValue, actual)
	}
}

func TestWellKnownBackendImplementation_getWellKnownUrlWithoutSlash(t *testing.T) {
	backend := WellKnownBackendImplementation{}
	actual := backend.getWellKnownUrl("prefix")

	expectedValue := "prefix/.well-known/config-seeder/client/global-config"
	if actual != expectedValue {
		t.Errorf("expected url to be %s but was %s", expectedValue, actual)
	}
}

func TestWellKnownBackendImplementation_createRequest(t *testing.T) {

	wellKnownUrl := "wellKnownUrl"
	apiKey := "apiKey"

	backend := WellKnownBackendImplementation{}
	actual, err := backend.createRequest(wellKnownUrl, apiKey)

	if err != nil {
		t.Errorf("createRequest() shouldn't return error: %s", err)
	}

	checkHeader("Accept", "application/json", actual.Header, t)
	checkHeader("Authorization", "Bearer "+apiKey, actual.Header, t)

	if actual.Method != "GET" {
		t.Errorf("Method should be GET but was %s", actual.Method)
	}

	if actual.URL == nil {
		t.Error("URL shouldn't be <nil>")
	}

	if actual.URL.Path != wellKnownUrl {
		t.Errorf("URL should be %s but was %s", wellKnownUrl, actual.URL.Path)
	}
}

func TestWellKnownBackendImplementation_unpackFromResponse(t *testing.T) {
	expectedLogo := "/assets/img/ConfigSeeder.svg"
	expectedBaseUrl := "https://staging-postgresql-config-seeder.oneo.cloud"
	expectedRestInternal := "/internal"
	expectedRestPublic := "/public/api/v1"

	responseJson := `{
	"urls": {
		"logo": "` + expectedLogo + `",
		"baseUrl": "` + expectedBaseUrl + `",
		"restInternal": "` + expectedRestInternal + `",
		"restPublic": "` + expectedRestPublic + `",
		"restExternal": null
	}
}`

	bodyCloser := io.NopCloser(bytes.NewReader([]byte(responseJson)))

	response := http.Response{
		Body:       bodyCloser,
		StatusCode: 200,
	}

	backend := WellKnownBackendImplementation{}
	wellKnown, err := backend.unpackFromResponse(&response)

	if err != nil {
		t.Errorf("getResponse() shouldn't return error: %s", err)
	}

	if wellKnown == nil {
		t.Error("wellKnown shouldn't be nil")
	}

	logoKey := "logo"
	if logoValue, ok := wellKnown.GetUrl(logoKey); !ok {
		t.Fatalf("Expected well known to contain %s but didn't", logoKey)
	} else if expectedLogo != logoValue {
		t.Errorf("Expected %s to be %s but was %s", logoKey, expectedLogo, logoValue)
	}
	baseUrlKey := "baseUrl"
	if baseUrlValue, ok := wellKnown.GetUrl(baseUrlKey); !ok {
		t.Fatalf("Expected well known to contain %s but didn't", baseUrlKey)
	} else if expectedBaseUrl != baseUrlValue {
		t.Errorf("Expected %s to be %s but was %s", baseUrlKey, expectedBaseUrl, baseUrlValue)
	}
	restInternalKey := "restInternal"
	if restInternalValue, ok := wellKnown.GetUrl(restInternalKey); !ok {
		t.Fatalf("Expected well known to contain %s but didn't", restInternalKey)
	} else if expectedRestInternal != restInternalValue {
		t.Errorf("Expected %s url to be %s but was %s", restInternalKey, expectedRestInternal, restInternalValue)
	}
	restPublicKey := "restPublic"
	if restPublicValue, ok := wellKnown.GetUrl(restPublicKey); !ok {
		t.Fatalf("Expected well known to contain %s but didn't", restPublicValue)
	} else if expectedRestPublic != restPublicValue {
		t.Errorf("Expected %s url to be %s but was %s", restPublicKey, expectedRestPublic, restPublicValue)
	}
	restExternalKey := "restExternal"
	if restExternalValue, ok := wellKnown.GetUrl(restExternalKey); ok {
		t.Fatalf("Expected well known to not contain rest external url but did (%s)", restExternalValue)
	}
}
