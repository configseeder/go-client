package backend

import (
	"errors"
	"fmt"
	"gitlab.com/configseeder/go-client/config"
	"net/http"
	"net/url"
	"testing"
	"time"
)

func Test_handleErrorTimeoutError(t *testing.T) {
	err := TimeoutError{}

	actual := handleError(&err, "url")

	if retryable, ok := actual.(*RetryableError); ok {
		cause := "i/o timeout"
		if retryable.Cause.Error() != cause {
			t.Errorf("cause should be %s but was %s", cause, retryable.Cause.Error())
		}
	} else {
		t.Error("error should be of type RetryableError")
	}
}

func Test_handleErrorUrlErrorTimeoutError(t *testing.T) {
	timeoutError := TimeoutError{}
	urlError := url.Error{Err: &timeoutError}

	actual := handleError(&urlError, "url")

	if retryable, ok := actual.(*RetryableError); ok {
		cause := "i/o timeout"
		if urlError, ok := retryable.Cause.(*url.Error); ok {
			if urlError.Err.Error() != cause {
				t.Errorf("cause should be %s but was %s", cause, retryable.Cause.Error())
			}
		} else {
			t.Error("causeError should be of type url.Error")
		}
	} else {
		t.Error("error should be of type RetryableError")
	}
}

func Test_handleError(t *testing.T) {
	message := "message"
	err := errors.New(message)
	someUrl := "someUrl"

	actual := handleError(err, someUrl)

	expectedMessage := fmt.Sprintf("Unable to access ConfigSeeder on url %s: %s", someUrl, message)
	if actual.Error() != expectedMessage {
		t.Errorf("cause should be %s but was %s", expectedMessage, actual.Error())
	}
}

func Test_handleErrorNil(t *testing.T) {
	someUrl := "someUrl"

	actual := handleError(nil, someUrl)

	if actual != nil {
		t.Errorf("handleError() shouldn't return <nil> if an error is passed")
	}
}

func Test_getHttpClient(t *testing.T) {
	connectionTimeout := 1
	readTimeout := 2

	configuration := config.ClientConfiguration{
		ConnectionTimeout: &connectionTimeout,
		ReadTimeout:       &readTimeout,
	}

	actual := getHttpClient(&configuration)

	if actual.Timeout != time.Duration(connectionTimeout)*time.Millisecond {
		t.Errorf("Expected Timeout to be %dms but got %s", connectionTimeout, actual.Timeout.String())
	}

	transport := actual.Transport.(*http.Transport)
	if transport.ResponseHeaderTimeout != time.Duration(readTimeout)*time.Millisecond {
		t.Errorf("Expected Timeout to be %dms but got %s", readTimeout, transport.ResponseHeaderTimeout.String())
	}
}

type TimeoutError struct{}

func (e *TimeoutError) Error() string   { return "i/o timeout" }
func (e *TimeoutError) Timeout() bool   { return true }
func (e *TimeoutError) Temporary() bool { return true }
