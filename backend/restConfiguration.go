package backend

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
	"io"
	"net/http"
	"strings"
)

type ConfigurationBackend interface {
	// GetConfigValuesFromConfigSeeder calls the ConfigSeeders configurations endpoint to retrieve the ConfigValues specified by the ConfigurationRequestDto.
	//
	GetConfigValuesFromConfigSeeder(configurationRequestDto *model.ConfigurationRequestDto, clientConfiguration *config.ClientConfiguration) (*[]model.ConfigValueDto, error)
}

type ConfigurationBackendImplementation struct {
	info util.UserAgentInfo
}

func InitConfiguration(info util.UserAgentInfo) ConfigurationBackend {
	var backend ConfigurationBackend = &ConfigurationBackendImplementation{
		info: info,
	}

	return backend
}

func (b *ConfigurationBackendImplementation) GetConfigValuesFromConfigSeeder(configurationRequestDto *model.ConfigurationRequestDto, clientConfiguration *config.ClientConfiguration) (*[]model.ConfigValueDto, error) {
	log.Debug("Starting to retrieve configuration values from ConfigSeeder")
	if configurationRequestDto == nil {
		return nil, errors.New("go client not properly initialized (call core.InitFlags(), flags.parse(), core.Init() to initialize go client)")
	}

	configurationsUrl := b.getConfigurationsUrl(*clientConfiguration.ServerUrl)

	httpRequest, err := b.createRequest(configurationRequestDto, configurationsUrl, *clientConfiguration.ApiKey)
	if err != nil {
		return nil, err
	}

	client := getHttpClient(clientConfiguration)

	response, err := client.Do(httpRequest)
	if response != nil {
		defer func(Body io.ReadCloser) {
		_:
			Body.Close()
		}(response.Body)
	}

	err = handleError(err, configurationsUrl)
	if err != nil {
		return nil, err
	}

	return b.unpackFromResponse(response)
}

func (b *ConfigurationBackendImplementation) getConfigurationsUrl(url string) string {
	propertiesUrl := url
	if !strings.HasSuffix(propertiesUrl, "/") {
		propertiesUrl += "/"
	}
	propertiesUrl += "public/api/v1/configurations"
	return propertiesUrl
}

// createGetConfigurationRequest creates the request object which is sent to ConfigSeeder for requesting the configurationsValues.
func (b *ConfigurationBackendImplementation) createRequest(configurationRequestDto *model.ConfigurationRequestDto, configurationsUrl string, apiKey string) (*http.Request, error) {
	requestString, err := json.Marshal(configurationRequestDto)
	if err != nil {
		message := fmt.Sprintf("Unable to serialise request to json: %s", err)
		return nil, errors.New(message)
	}

	httpRequest, err := http.NewRequest("POST", configurationsUrl, bytes.NewBuffer(requestString))
	if err != nil {
		message := fmt.Sprintf("Unable to create POST request for %s: %s", configurationsUrl, err)
		return nil, errors.New(message)
	}

	httpRequest.Header.Add("Accept", "application/metadata+json")
	httpRequest.Header.Add("Content-Type", "application/json; charset=UTF-8")
	httpRequest.Header.Add("Authorization", "Bearer "+apiKey)

	util.AddCustomHeadersToRequest(httpRequest, &b.info)

	httpRequest.Close = true

	return httpRequest, nil
}

// unpackFromResponse gets the requested Slice of ConfigValueDto's out of the http.Response.
func (b *ConfigurationBackendImplementation) unpackFromResponse(response *http.Response) (*[]model.ConfigValueDto, error) {
	responseJson, err := io.ReadAll(response.Body)
	if err != nil {
		message := fmt.Sprintf("Unable to read body from response: %s", err)
		return nil, errors.New(message)
	}

	if http.StatusOK != response.StatusCode {
		message := fmt.Sprintf("ConfigServer returned unexpectet status %s, body: %s", response.Status, responseJson)
		return nil, errors.New(message)
	}

	propertyGroupResponse := &[]model.ConfigValueDto{}
	err = json.Unmarshal(responseJson, propertyGroupResponse)

	if err != nil {
		message := fmt.Sprintf("Unable to deserialise json to PropertyGroupResponse: (%v) %s", response, err)
		return nil, errors.New(message)
	}

	return propertyGroupResponse, nil
}
