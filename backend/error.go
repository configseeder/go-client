package backend

import "fmt"

/*
 * A RetryableError marks a temporary error. Accessing the ConfigSeeder should be retried.
 */
type RetryableError struct {
	Cause error
}

func (e *RetryableError) Error() string {
	return fmt.Sprintf("Cause: %s", e.Cause)
}
