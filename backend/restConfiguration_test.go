package backend

import (
	"bytes"
	"fmt"
	"gitlab.com/configseeder/go-client/model"
	"io"
	"net/http"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestConfigurationBackendImplementation_getConfigurationsUrlWithSlash(t *testing.T) {
	backend := ConfigurationBackendImplementation{}
	actual := backend.getConfigurationsUrl("prefix/")

	expectedValue := "prefix/public/api/v1/configurations"
	if actual != expectedValue {
		t.Errorf("expected url to be %s but was %s", expectedValue, actual)
	}
}

func TestConfigurationBackendImplementation_getConfigurationsUrlWithoutSlash(t *testing.T) {
	backend := ConfigurationBackendImplementation{}
	actual := backend.getConfigurationsUrl("prefix")

	expectedValue := "prefix/public/api/v1/configurations"
	if actual != expectedValue {
		t.Errorf("expected url to be %s but was %s", expectedValue, actual)
	}
}

func TestConfigurationBackendImplementation_createRequestEmpty(t *testing.T) {
	expectedJson := "{\"fullForNonFiltered\":false,\"configurations\":null}"

	requestDto := model.ConfigurationRequestDto{}

	getConfigurationUrl := "getConfigurationUrl"
	apiKey := "apiKey"

	backend := ConfigurationBackendImplementation{}
	actual, err := backend.createRequest(&requestDto, getConfigurationUrl, apiKey)

	if err != nil {
		t.Errorf("createRequest() shouldn't return error: %s", err)
	}

	checkHeader("Accept", "application/metadata+json", actual.Header, t)
	checkHeader("Content-Type", "application/json; charset=UTF-8", actual.Header, t)
	checkHeader("Authorization", "Bearer "+apiKey, actual.Header, t)
	checkBody(actual, expectedJson, t)

	if actual.Method != "POST" {
		t.Errorf("Method should be POST but was %s", actual.Method)
	}

	if actual.URL == nil {
		t.Error("URL shouldn't be <nil>")
	}

	if actual.URL.Path != getConfigurationUrl {
		t.Errorf("URL should be %s but was %s", getConfigurationUrl, actual.URL.Path)
	}
}

func TestConfigurationBackendImplementation_createRequest(t *testing.T) {
	dateTimeString := "2001-02-03T05:06:07Z"
	dateTime, _ := time.Parse(time.RFC3339, dateTimeString)
	tenantKey := "tenantKey"
	environmentKey := "environmentKey"
	context := "context"
	version := "version"

	expectedJson := fmt.Sprintf("{\"dateTime\":\"%s\",\"tenantKey\":\"%s\",\"environmentKey\":\"%s\",\"fullForNonFiltered\":false,\"context\":\"%s\",\"version\":\"%s\",\"configurations\":null}",
		dateTimeString, tenantKey, environmentKey, context, version)

	requestDto := model.ConfigurationRequestDto{
		TenantKey:      tenantKey,
		EnvironmentKey: &environmentKey,
		Context:        &context,
		Version:        &version,
		DateTime:       &dateTime,
	}

	getConfigurationUrl := "getConfigurationUrl"
	apiKey := "apiKey"

	backend := ConfigurationBackendImplementation{}
	actual, err := backend.createRequest(&requestDto, getConfigurationUrl, apiKey)

	if err != nil {
		t.Errorf("createRequest() shouldn't return error: %s", err)
	}

	checkHeader("Accept", "application/metadata+json", actual.Header, t)
	checkHeader("Content-Type", "application/json; charset=UTF-8", actual.Header, t)
	checkHeader("Authorization", "Bearer "+apiKey, actual.Header, t)
	checkBody(actual, expectedJson, t)

	if actual.Method != "POST" {
		t.Errorf("Method should be POST but was %s", actual.Method)
	}

	if actual.URL == nil {
		t.Error("URL shouldn't be <nil>")
	}

	if actual.URL.Path != getConfigurationUrl {
		t.Errorf("URL should be %s but was %s", getConfigurationUrl, actual.URL.Path)
	}
}

func checkHeader(headerName string, expectedValue string, header http.Header, t *testing.T) {
	headerValue := header.Get(headerName)

	if expectedValue != headerValue {
		t.Errorf("Expected header %s to be %s but was %s", headerName, expectedValue, headerValue)
	}
}

func checkBody(request *http.Request, expectedJson string, t *testing.T) {
	responseJson, err := io.ReadAll(request.Body)
	if err != nil {
		t.Errorf("reading body shouldn't return error: %s", err)
	}
	if expectedJson != string(responseJson) {
		t.Errorf("Expected body to be %s but was %s", expectedJson, string(responseJson))
	}
}

func TestConfigurationBackendImplementation_unpackConfigValuesFromResponse(t *testing.T) {
	key := "key"
	value := "value"
	configurationValueId := "configurationValueId"
	environment := "environment"
	environmentId := "environmentId"
	millies := 1544649977295
	lastUpdate := "2018-12-12T21:26:17.295"

	responseJson := `[
    {
        "key": "` + key + `",
        "value": "` + value + `",
        "type": "STRING",
        "configurationValueId": "` + configurationValueId + `",
        "secured": false,
        "environment": "` + environment + `",
        "environmentId": "` + environmentId + `",
        "validFrom": null,
        "validTo": null,
        "versionFrom": null,
        "versionTo": null,
        "context": null,
        "lastUpdateInMilliseconds": ` + strconv.Itoa(millies) + `,
        "lastUpdate": "` + lastUpdate + `"
    }
]`

	bodyCloser := io.NopCloser(bytes.NewReader([]byte(responseJson)))

	response := http.Response{
		Body:       bodyCloser,
		StatusCode: 200,
	}

	backend := ConfigurationBackendImplementation{}
	configValues, err := backend.unpackFromResponse(&response)

	if err != nil {
		t.Errorf("getResponse() shouldn't return error: %s", err)
	}

	if configValues == nil {
		t.Error("configValues shouldn't be nil")
	}

	expectedLength := 1
	if len(*configValues) != expectedLength {
		t.Fatalf("Expected configValues to contain %d elements but got %d",
			expectedLength, len(*configValues))
	}

	configValue := (*configValues)[0]

	if configValue.Key != key {
		t.Errorf("Expected key to be %s but was %s", key, configValue.Key)
	}
	if configValue.Value != value {
		t.Errorf("Expected value to be %s but was %s", value, configValue.Value)
	}
	if configValue.ConfigurationValueId != configurationValueId {
		t.Errorf("Expected configurationValueId to be %s but was %s", configurationValueId, configValue.ConfigurationValueId)
	}
	if configValue.EnvironmentId == nil {
		t.Error("environmentId should not be nil")
	} else if *configValue.EnvironmentId != environmentId {
		t.Errorf("Expected environmentId to be %s but was %s", environmentId, *configValue.EnvironmentId)
	}
	if configValue.Environment == nil {
		t.Error("environment should not be nil")
	} else if *configValue.Environment != environment {
		t.Errorf("Expected environment to be %s but was %s", environment, *configValue.Environment)
	}
	if configValue.LastUpdateInMilliseconds != int64(millies) {
		t.Errorf("Expected lastUpdateInMilliseconds to be %d but was %d", millies, configValue.LastUpdateInMilliseconds)
	}
	if configValue.LastUpdate != lastUpdate {
		t.Errorf("Expected lastUpdate to be %s but was %s", lastUpdate, configValue.LastUpdate)
	}
}

func TestConfigurationBackendImplementation_unpackFromResponseError(t *testing.T) {
	responseJson := `{
    "Error": "error"
}`

	bodyCloser := io.NopCloser(bytes.NewReader([]byte(responseJson)))

	response := http.Response{
		Body:       bodyCloser,
		StatusCode: 500,
	}

	backend := ConfigurationBackendImplementation{}
	configValues, err := backend.unpackFromResponse(&response)

	if err == nil {
		t.Errorf("getResponse() shouldt return error: %s", err)
	}
	if strings.Index(err.Error(), responseJson) == -1 {
		t.Errorf("error should contain transmitted error but didn't (%s)", err)
	}

	if configValues != nil {
		t.Error("configValues should be nil")
	}
}
