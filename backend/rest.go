package backend

import (
	"crypto/tls"
	"errors"
	"fmt"
	"gitlab.com/configseeder/go-client/config"
	"net"
	"net/http"
	"net/url"
	"time"
)

// getHttpClient returns a http client with timeout configuration.
//
func getHttpClient(clientConfiguration *config.ClientConfiguration) http.Client {
	transport := http.Transport{
		ResponseHeaderTimeout: time.Duration(*clientConfiguration.ReadTimeout) * time.Millisecond,
	}

	if clientConfiguration.CertificatePool != nil {
		transport.TLSClientConfig = &tls.Config{
			RootCAs: clientConfiguration.CertificatePool,
		}
	}

	client := http.Client{
		Timeout:   time.Duration(*clientConfiguration.ConnectionTimeout) * time.Millisecond,
		Transport: &transport,
	}
	return client
}

// handleError checks if the given error is a retrieable error (like a timeout) or a permanent error.
// For if possible, a retrieable error is returned
//
func handleError(error error, configurationsUrl string) error {
	switch err := error.(type) {
	case *url.Error:
		if _, ok := err.Err.(net.Error); ok && err.Timeout() {
			return &RetryableError{
				Cause: err,
			}
		}
	case net.Error:
		if err.Timeout() {
			return &RetryableError{
				Cause: err,
			}
		}
	}

	if error != nil {
		message := fmt.Sprintf("Unable to access ConfigSeeder on url %s: %s", configurationsUrl, error)
		return errors.New(message)
	}

	return nil
}
