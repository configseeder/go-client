package util

import (
	"testing"
	"time"
)

func TestTimeInMilliseconds(t *testing.T) {
	dateTime := time.Unix(10002000, 300000000)
	actual := TimeInMilliseconds(dateTime)

	var expectedMs int64 = 10002000300
	if actual != expectedMs {
		t.Errorf("Expected %d milliseconds but got %d", expectedMs, actual)
	}
}

func TestMakeTimestamp(t *testing.T) {

	actual := MakeTimestamp()

	expected := time.Now().Round(time.Millisecond).UnixNano() / 1000000
	difference := expected - actual
	if difference > 100 {
		t.Errorf("Expected timestamp to be %d (max + 100) but was %d (difference: %d)",
			expected, actual, difference)
	}
}
