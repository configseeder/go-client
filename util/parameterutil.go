package util

import (
	"flag"
	log "github.com/sirupsen/logrus"
	"os"
	"strconv"
	"strings"
	"time"
)

/*
 * Loads the parameter defined by key.
 *
 * First priority is parameter defined by flag (-<key>=value)
 * Second priority is parameter defined by environment variable (key is transformed to SNAKE_CASE)
 *
 * Returns value of parameter and flag, if parameter is set (false = parameter not found, true = parameter found)
 */
func GetParameter(key string) (string, bool) {
	parameterValue := flag.Lookup(key)

	if parameterValue != nil && len(parameterValue.Value.String()) > 0 {
		return parameterValue.Value.String(), true
	}

	envKey := strings.Replace(key, ".", "_", -1)
	envValue, exists := os.LookupEnv(strings.ToUpper(envKey))

	if exists {
		return strings.TrimSpace(envValue), true
	}

	return "", false
}

/*
 * Loads the bool-parameter defined by key.
 *
 * First priority is parameter defined by flag (-<key>=value)
 * Second priority is parameter defined by environment variable (key is transformed to SNAKE_CASE)
 *
 * Returns value of parameter and flag, if parameter is set (false = parameter not found, true = parameter found)
 */
func GetBoolParameter(key string) (bool, bool) {
	value, exists := GetParameter(key)

	if exists {
		boolValue, err := strconv.ParseBool(value)
		if err != nil {
			log.Errorf("Parameter %s must be of type bool: %s", key, err)
			return false, false
		}

		return boolValue, true
	}
	return false, false
}

/*
 * Loads the int-parameter defined by key.
 *
 * First priority is parameter defined by flag (-<key>=value)
 * Second priority is parameter defined by environment variable (key is transformed to SNAKE_CASE)
 *
 * Returns value of parameter and flag, if parameter is set (false = parameter not found, true = parameter found)
 */
func GetIntParameter(key string) (int, bool) {
	value, exists := GetParameter(key)

	if exists {
		intValue, err := strconv.Atoi(value)
		if err != nil {
			log.Errorf("Parameter %s must be of type int: %s", key, err)
			return 0, false
		}

		return intValue, true
	}
	return 0, false
}

/*
 * Loads the time-parameter defined by key.
 *
 * First priority is parameter defined by flag (-<key>=value)
 * Second priority is parameter defined by environment variable (key is transformed to SNAKE_CASE)
 *
 * Returns value of parameter and flag, if parameter is set (false = parameter not found, true = parameter found)
 */
func GetDateTimeParameter(key string) (*time.Time, bool) {
	value, exists := GetParameter(key)

	if exists {
		timeValue, err := time.Parse(time.RFC3339, value)
		if err != nil {
			log.Errorf("Parameter %s must be of type time (format: %s): %s", key, time.RFC3339, err)
			return nil, false
		}

		return &timeValue, true
	}
	return nil, false
}

/*
 * Loads the duration-parameter defined by key.
 *
 * First priority is parameter defined by flag (-<key>=value)
 * Second priority is parameter defined by environment variable (key is transformed to SNAKE_CASE)
 *
 * Returns value of parameter and flag, if parameter is set (false = parameter not found, true = parameter found)
 */
func GetDurationParameter(key string) (*time.Duration, bool) {
	value, exists := GetParameter(key)

	if exists {
		durationValue, err := time.ParseDuration(value)
		if err != nil {
			log.Errorf("Parameter %s must be of type duration: %s", key, err)
			return nil, false
		}

		return &durationValue, true
	}

	return nil, false
}
