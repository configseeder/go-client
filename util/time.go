package util

import "time"

/*
 * Converts the given time to milliseconds.
 */
func TimeInMilliseconds(dateTime time.Time) int64 {
	return dateTime.Round(time.Millisecond).UnixNano() / 1000000
}

/*
 * Returns the current time in milliseconds.
 */
func MakeTimestamp() int64 {
	return TimeInMilliseconds(time.Now())
}
