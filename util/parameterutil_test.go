package util

import (
	"flag"
	"gitlab.com/configseeder/go-client/util_test"
	"os"
	"strings"
	"testing"
)

const KEY = "key"

var properties = map[string]string{
	KEY: "true",
}

func TestGetParameterUnset(t *testing.T) {
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Parse()

	actual, ok := GetParameter(KEY)

	if ok {
		t.Errorf("Flag %s should not be available (as env or flag)", KEY)
	}

	if len(actual) > 0 {
		t.Errorf("Value of Flag %s should not \"\" but was %s", KEY, actual)
	}
}

func TestGetParameterFlag(t *testing.T) {
	value := "value"

	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Set(KEY, value)
	flag.Parse()

	actual, ok := GetParameter(KEY)

	if !ok {
		t.Errorf("Flag %s should be available (as flag)", KEY)
	}

	if value != actual {
		t.Errorf("Value of Flag %s should be %s but was %s", KEY, value, actual)
	}
}

func TestGetParameterEnv(t *testing.T) {
	value := "value"

	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Parse()

	os.Setenv(strings.ToUpper(KEY), value)

	actual, ok := GetParameter(KEY)

	if !ok {
		t.Errorf("Flag %s should be available (as env)", KEY)
	}

	if value != actual {
		t.Errorf("Value of Flag %s should be %s but was %s", KEY, value, actual)
	}
}

func TestGetParameterEnvTrimmed(t *testing.T) {
	value := `value
`
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Parse()

	os.Setenv(strings.ToUpper(KEY), value)

	actual, ok := GetParameter(KEY)

	if !ok {
		t.Errorf("Flag %s should be available (as env)", KEY)
	}

	expectedValue := "value"
	if expectedValue != actual {
		t.Errorf("Value of Flag %s should be %s but was '%s'", KEY, expectedValue, actual)
	}
}

func TestGetBoolParameterNil(t *testing.T) {
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Parse()

	actual, ok := GetBoolParameter(KEY)

	if ok {
		t.Errorf("Flag %s should not be available (as env or flag)", KEY)
	}

	if actual {
		t.Errorf("Value of Flag %s should be \"false\" but was %t", KEY, actual)
	}
}

func TestGetBoolParameterInvalid(t *testing.T) {
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Set(KEY, "invalid")
	flag.Parse()

	actual, ok := GetBoolParameter(KEY)

	if ok {
		t.Errorf("Flag %s should not be available (invalid value)", KEY)
	}

	if actual {
		t.Errorf("Value of Flag %s should be \"false\" but was %t", KEY, actual)
	}
}

func TestGetBoolParameter(t *testing.T) {
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Set(KEY, "true")
	flag.Parse()

	actual, ok := GetBoolParameter(KEY)

	if !ok {
		t.Errorf("Flag %s should be available (as flag)", KEY)
	}

	if !actual {
		t.Errorf("Value of Flag %s should not \"true\" but was %t", KEY, actual)
	}
}

func TestGetIntParameterNil(t *testing.T) {
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Parse()

	actual, ok := GetIntParameter(KEY)

	if ok {
		t.Errorf("Flag %s should not be available (as env or flag)", KEY)
	}

	expected := 0
	if actual != expected {
		t.Errorf("Value of Flag %s should be %d but was %d", KEY, expected, actual)
	}
}

func TestGetIntParameterInvalid(t *testing.T) {
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Set(KEY, "invalid")
	flag.Parse()

	actual, ok := GetIntParameter(KEY)

	if ok {
		t.Errorf("Flag %s should not be available (invalid value)", KEY)
	}

	if actual != 0 {
		t.Errorf("Value of Flag %s should be \"0\" but was %d", KEY, actual)
	}
}

func TestGetIntParameter(t *testing.T) {
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Set(KEY, "3")
	flag.Parse()

	actual, ok := GetIntParameter(KEY)

	if !ok {
		t.Errorf("Flag %s should be available (as flag)", KEY)
	}

	expected := 3
	if actual != expected {
		t.Errorf("Value of Flag %s should be %d but was %d", KEY, expected, actual)
	}
}

func TestGetDateTimeParameterNil(t *testing.T) {
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Parse()

	actual, ok := GetDateTimeParameter(KEY)

	if ok {
		t.Errorf("Flag %s should not be available (as env or flag)", KEY)
	}

	if actual != nil {
		t.Errorf("Value of Flag %s should be nil but was %s", KEY, actual.String())
	}
}

func TestGetDateTimeParameterInvalid(t *testing.T) {
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Set(KEY, "invalid")
	flag.Parse()

	actual, ok := GetDateTimeParameter(KEY)

	if ok {
		t.Errorf("Flag %s should not be available (invalid value)", KEY)
	}

	if actual != nil {
		t.Errorf("Value for Flag %s should be <nil> but was %s", KEY, actual.String())
	}
}

func TestGetDateTimeParameter(t *testing.T) {
	input := "2018-01-02T03:04:05Z"
	expected := "2018-01-02 03:04:05 +0000 UTC"

	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Set(KEY, input)
	flag.Parse()

	actual, ok := GetDateTimeParameter(KEY)

	if !ok {
		t.Errorf("Flag %s should be available (as flag)", KEY)
	}

	if actual.String() != expected {
		t.Errorf("Value of Flag %s should be %s but was %s", KEY, expected, actual.String())
	}
}

func TestGetDurationParameterNil(t *testing.T) {
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Parse()

	actual, ok := GetDurationParameter(KEY)

	if ok {
		t.Errorf("Flag %s should not be available (as env or flag)", KEY)
	}

	if actual != nil {
		t.Errorf("Value of Flag %s should be nil but was %s", KEY, actual.String())
	}
}

func TestGetDurationParameterInvalid(t *testing.T) {
	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Set(KEY, "invalid")
	flag.Parse()

	actual, ok := GetDurationParameter(KEY)

	if ok {
		t.Errorf("Flag %s should not be available (invalid value)", KEY)
	}

	if actual != nil {
		t.Errorf("Value for Flag %s should be <nil> but was %s", KEY, actual.String())
	}
}

func TestGetDurationParameter(t *testing.T) {
	input := "5m"
	expected := "5m0s"

	util_test.ResetFlagsAndEnvironment(properties)

	flag.String(KEY, "", "the flag")
	flag.Set(KEY, input)
	flag.Parse()

	actual, ok := GetDurationParameter(KEY)

	if !ok {
		t.Errorf("Flag %s should be available (as flag)", KEY)
	}

	if actual.String() != expected {
		t.Errorf("Value of Flag %s should be %s but was %s", KEY, expected, actual.String())
	}
}
