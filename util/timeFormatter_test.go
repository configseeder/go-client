package util

import (
	"testing"
	"time"
)

func TestFormattetTimeToTimeNil(t *testing.T) {
	var formattetTime *string = nil
	actual, err := FormattetTimeToTime(formattetTime)

	if err != nil {
		t.Errorf("nil should be converted to nil without error but got: %s", err)
	}

	if actual != nil {
		t.Errorf("Expected time to be <nil> but was %s", actual.String())
	}
}

func TestFormattetTimeToTime(t *testing.T) {
	var stringTime string = "2018-01-02T03:04:05"
	actual, err := FormattetTimeToTime(&stringTime)

	if err != nil {
		t.Errorf("nil should be converted to nil without error but got: %s", err)
	}

	if actual == nil {
		t.Fatal("Converted time shouldn't be nil")
	}

	location, _ := time.LoadLocation("UTC")
	expectedTime := time.Date(2018, 1, 2, 3, 4, 5, 0, location)
	if *actual != expectedTime {
		t.Errorf("Expected time to be %s but was %s", expectedTime.String(), actual.String())
	}
}

func TestTimeToFormattetTimeNil(t *testing.T) {
	var nullTime *time.Time = nil
	actual := TimeToFormattetTime(nullTime)

	if actual != nil {
		t.Errorf("Expected time to be <nil> but was %s", *actual)
	}

}

func TestTimeToFormattetTime(t *testing.T) {
	location, _ := time.LoadLocation("UTC")
	dateTime := time.Date(2018, 1, 2, 3, 4, 5, 0, location)

	actual := TimeToFormattetTime(&dateTime)

	if actual == nil {
		t.Fatal("Converted time shouldn't be nil")
	}

	expectedTime := "2018-01-02T03:04:05"
	if *actual != expectedTime {
		t.Errorf("Expected time to be %s but was %s", expectedTime, *actual)
	}
}

func TestFormattetTimeWithZoneToTime_Nil(t *testing.T) {
	var formattetTime *string = nil
	actual, err := FormattetTimeWithZoneToTime(formattetTime)

	if err != nil {
		t.Errorf("nil should be converted to nil without error but got: %s", err)
	}

	if actual != nil {
		t.Errorf("Expected time to be <nil> but was %s", actual.String())
	}
}

func TestFormattetTimeWithZoneToTime(t *testing.T) {
	var stringTime string = "2018-01-02T03:04:05Z"
	actual, err := FormattetTimeWithZoneToTime(&stringTime)

	if err != nil {
		t.Errorf("nil should be converted to nil without error but got: %s", err)
	}

	if actual == nil {
		t.Fatal("Converted time shouldn't be nil")
	}

	location, _ := time.LoadLocation("UTC")
	expectedTime := time.Date(2018, 1, 2, 3, 4, 5, 0, location)
	if *actual != expectedTime {
		t.Errorf("Expected time to be %s but was %s", expectedTime.String(), actual.String())
	}
}

func TestTimeToFormattetTimeWithZone_Nil(t *testing.T) {
	var nullTime *time.Time = nil
	actual := TimeToFormattetTimeWithZone(nullTime)

	if actual != nil {
		t.Errorf("Expected time to be <nil> but was %s", *actual)
	}

}

func TestTimeToFormattetTimeWithZone(t *testing.T) {
	location, _ := time.LoadLocation("UTC")
	dateTime := time.Date(2018, 1, 2, 3, 4, 5, 0, location)

	actual := TimeToFormattetTimeWithZone(&dateTime)

	if actual == nil {
		t.Fatal("Converted time shouldn't be nil")
	}

	expectedTime := "2018-01-02T03:04:05Z"
	if *actual != expectedTime {
		t.Errorf("Expected time to be %s but was %s", expectedTime, *actual)
	}
}

func TestDurationToFormattetDurationNil(t *testing.T) {
	actual := DurationToFormattetDuration(nil)

	if actual != nil {
		t.Errorf("Expected duration to be nil but was %s", *actual)
	}
}

func TestDurationToFormattetDuration(t *testing.T) {
	duration, _ := time.ParseDuration("5m")

	actual := DurationToFormattetDuration(&duration)

	if actual == nil {
		t.Fatal("Duration shouldn't be nil")
	}

	expectedDuration := "5m0s"
	if *actual != expectedDuration {
		t.Errorf("Expected duration to be %s but was %s", expectedDuration, *actual)
	}
}
