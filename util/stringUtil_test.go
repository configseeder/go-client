package util

import "testing"

func TestGetStringValueNil(t *testing.T) {
	var input *string

	actual := GetStringValue(input)

	if actual != "" {
		t.Errorf("Expected time to be \"\" but was %s", actual)
	}
}

func TestGetStringValue(t *testing.T) {
	input := "abc"

	actual := GetStringValue(&input)

	if actual != input {
		t.Errorf("Expected time to be %s but was %s", input, actual)
	}
}
