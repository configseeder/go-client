package util

import (
	"crypto/x509"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"runtime"
)

func InitCertificatePool(certificateFilename *string) (*x509.CertPool, error) {
	if certificateFilename == nil {
		// No custom certificate specified, do nothing
		return nil, nil
	}

	certPool, err := getCertPool()
	if err != nil {
		return nil, err
	}

	certificatePem, err := ioutil.ReadFile(*certificateFilename)
	if err != nil {
		return nil, err
	}

	ok := certPool.AppendCertsFromPEM(certificatePem)
	if ok {
		log.Infof("Added certificates from file %s to certificate pool", *certificateFilename)
	} else {
		log.Warnf("Unable to add certificates from file %s to certificate pool", *certificateFilename)
	}

	return certPool, nil
}

func getCertPool() (*x509.CertPool, error) {
	if runtime.GOARCH == "windows" {
		return x509.NewCertPool(), nil
	} else {
		return x509.SystemCertPool()
	}
}
