package util

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"net"
	"net/http"
	"os"
	"runtime"
)

type UserAgentInfo struct {
	Hostname       string
	Component      string
	Subcomponent   string
	Version        string
	Os             string
	Identification string

	// calculated values
	userAgent          *string
	identificationHash *string
}

/*
 * Returns a reference to a valid UserAgentInfo.
 * If info is already set, info is just returned.
 * Otherwise, the default info for the go-client is created.
 */
func GetUserAgentInfo(info *UserAgentInfo) (*UserAgentInfo, error) {
	if info != nil {
		return info, nil
	}

	hostname, err := os.Hostname()
	if err != nil {
		return nil, fmt.Errorf("unable to create UserAgentInfo: %s", err)
	}

	identification := "hostname/" + GetMacAddress()

	info = &UserAgentInfo{
		Component:      "ConfigSeederClient",
		Subcomponent:   "Go",
		Version:        GoClientVersion,
		Hostname:       hostname,
		Identification: identification,
		Os:             runtime.GOOS,
	}

	return info, nil
}

func calculateValuesIfRequired(info *UserAgentInfo) {
	if info.identificationHash == nil {
		hashBytes := sha256.Sum256([]byte(info.Identification))
		hashString := fmt.Sprintf("%x", hashBytes)
		info.identificationHash = &hashString
	}

	if info.userAgent == nil {
		subcomponent := ""
		if len(info.Subcomponent) > 0 {
			subcomponent = "-" + info.Subcomponent
		}
		userAgent := fmt.Sprintf("%s%s/%s (%s)", info.Component, subcomponent, info.Version, info.Os)
		info.userAgent = &userAgent
	}
}

func AddCustomHeadersToRequest(request *http.Request, info *UserAgentInfo) {
	calculateValuesIfRequired(info)

	request.Header.Add("X-Host-Identification", *info.identificationHash)
	request.Header.Add("X-Hostname", info.Hostname)
	request.Header.Add("X-User-Agent", *info.userAgent)
}

func GetMacAddress() string {
	interfaces, err := net.Interfaces()
	if err == nil {
		for _, i := range interfaces {
			if i.Flags&net.FlagUp != 0 && bytes.Compare(i.HardwareAddr, nil) != 0 {
				// Don't use random as we have a real address
				return i.HardwareAddr.String()
			}
		}
	}
	return ""
}
