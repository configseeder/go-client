package util

import "time"

const DateTimeLayout = "2006-01-02T15:04:05"

func FormattetTimeToTime(originalTime *string) (*time.Time, error) {
	if originalTime == nil {
		return nil, nil
	}

	timetime, err := time.Parse(DateTimeLayout, *originalTime)
	if err != nil {
		return nil, err
	}

	return &timetime, nil
}

func TimeToFormattetTime(originalTime *time.Time) *string {
	if originalTime == nil {
		return nil
	} else {
		formattetTime := originalTime.Format(DateTimeLayout)
		return &formattetTime
	}
}

func FormattetTimeWithZoneToTime(originalTime *string) (*time.Time, error) {
	if originalTime == nil {
		return nil, nil
	}

	timetime, err := time.Parse(time.RFC3339, *originalTime)
	if err != nil {
		return nil, err
	}

	return &timetime, nil
}

func TimeToFormattetTimeWithZone(originalTime *time.Time) *string {
	if originalTime == nil {
		return nil
	} else {
		formattetTime := originalTime.Format(time.RFC3339)
		return &formattetTime
	}
}

func DurationToFormattetDuration(originalDuration *time.Duration) *string {
	if originalDuration == nil {
		return nil
	} else {
		formattedDuration := originalDuration.String()
		return &formattedDuration
	}
}
