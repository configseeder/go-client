# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.5.2] - 2024-12-07

### Changed

- Updated dependencies [#53](https://gitlab.com/configseeder/go-client/-/issues/53)

## [1.5.0] - 2024-05-09

### Added

- Added support for accessing configuration values with labels [#52](https://gitlab.com/configseeder/go-client/-/issues/52)
- Updated to golang 1.22.3

## [1.4.2] - 2023-12-07

### Added

- Added configSeeder version to the WellKnown object [#51](https://gitlab.com/configseeder/go-client/-/issues/51)

## [1.4.1] - 2023-11-10

### Changed

- Update dependencies (fix resetting flags for testing) [#49](https://gitlab.com/configseeder/go-client/-/issues/49)

## [1.4.0] - 2023-10-04

### Changed

- Update dependencies (golang 1.21.1, logrus 1.9) [#49](https://gitlab.com/configseeder/go-client/-/issues/49)

## [1.3.1] - 2022-02-11

### Added

- Retrieve URLs exposed on ConfigSeeders well known endpoint [#47](https://gitlab.com/configseeder/go-client/issues/47)

## [1.3.0] - 2021-10-26

### Added

- Support ConfigValueDto.AdditionalParameters [#45](https://gitlab.com/configseeder/go-client/issues/45)

### Changed

- Update to golang 1.17 [#46](https://gitlab.com/configseeder/go-client/issues/46)

## [1.2.1] - 2021-04-19

### Changed

- Update to golang 1.16 [#44](https://gitlab.com/configseeder/go-client/issues/44)

## [1.2.0] - 2021-01-28

### Changed

- Update to golang 1.15 and go mod dependency management [#43](https://gitlab.com/configseeder/go-client/issues/43)

## [1.1.0] - 2020-12-18

### Added

- Support TSL verification with custom ca certificate / chain [#41](https://gitlab.com/configseeder/go-client/issues/41)

## [1.0.1] - 2020-04-20

### Added

- Support for data types CERTIFICATE and PRIVATE_KEY [#34](https://gitlab.com/configseeder/go-client/issues/34)
- Support for data types TEMPLATE [#37](https://gitlab.com/configseeder/go-client/issues/37)

### Breaking Change

- Use Times with timezone [#36](https://gitlab.com/configseeder/go-client/-/issues/36)

  The `dateTime` filter must be specified with timezone. Example `2001-02-03T04:05:06.789Z`

## [1.0.0] - 2020-02-14

### Added

- Support for data types CONFIGURATION_GROUP, ENUM, REGEX [#21](https://gitlab.com/configseeder/go-client/issues/21).

## [0.10.0] - 2019-10-06

This version is a Release Candidate. Do not use in Production!

Only use this version of go-client with _ConfigSeeder_ Management Versions >= 2.8.0.

### Breaking Change

- Change contextpath to `public/api/v1/`
