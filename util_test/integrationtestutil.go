package util_test

import (
	"context"
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

func InitServer(handler func(http.ResponseWriter, *http.Request)) {
	http.DefaultServeMux = http.NewServeMux()
	http.HandleFunc("/public/api/v1/configurations", handler)
	http.HandleFunc("/.well-known/config-seeder/client/global-config", handler)
}

func StartServer(port string) *http.Server {
	log.Infof("Starting ConfigSeeder Simulator on Port %s", port)

	server := http.Server{Addr: ":" + port}
	go func() {
		err := server.ListenAndServe()
		if err != nil {
			log.Printf("Unable to start server: %s", err)
		}
	}()

	time.Sleep(time.Millisecond * 50)

	return &server
}

func StopServer(server *http.Server) {
	if err := server.Shutdown(context.Background()); err != nil {
		panic("Unable to shut down server: " + err.Error())
	}
}
