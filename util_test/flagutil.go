package util_test

import (
	"flag"
	"os"
	"strings"
)

// ResetFlagsAndEnvironment Reset command line (remove all previously configured flags) and add all flags which are necessary for unit testing.
// Removes all environment variables specified by properties.
func ResetFlagsAndEnvironment(properties map[string]string) {
	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	flag.Bool("test.run", true, "")
	flag.String("test.v", "", "")
	flag.String("test.coverprofile", "", "")
	flag.String("test.gocoverdir", "", "")
	flag.Bool("test.paniconexit0", false, "")
	flag.String("test.testlogfile", "", "")
	flag.String("test.timeout", "", "")

	for k := range properties {
		envKey := strings.Replace(k, ".", "_", -1)
		_ = os.Unsetenv(strings.ToUpper(envKey))
	}
}
