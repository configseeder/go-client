package util_test

import (
	"testing"
	"time"
)

func GetStringValue(value *string) string {
	if value == nil {
		return "<nil>"
	}
	return *value
}

func CheckContainsKeyWithNull(dataMap map[string]interface{}, key string, t *testing.T) {
	if actualValue, ok := dataMap[key]; !ok {
		t.Errorf("Generated json should contain key %s", key)
	} else if actualValue != nil {
		t.Errorf("Value of key %s should be <nil> but was '%s'", key, actualValue)
	}
}

func CheckContainsKeyNotNull(dataMap map[string]interface{}, key string, t *testing.T) {
	if actualValue, ok := dataMap[key]; !ok {
		t.Errorf("Generated json should contain key %s", key)
	} else if actualValue == nil {
		t.Errorf("Value of key %s should not be <nil>", key)
	}
}

func CheckContainsKeyWithStringValue(dataMap map[string]interface{}, key string, expectedValue string, t *testing.T) {
	if actualValue, ok := dataMap[key]; !ok {
		t.Errorf("Generated json should contain key %s", key)
	} else if actualValue != expectedValue {
		t.Errorf("Value of key %s should be '%s' but was '%s'", key, expectedValue, actualValue)
	}
}

func CheckContainsKeyWithStringArrayValue(dataMap map[string]interface{}, key string, expectedValue string, t *testing.T) {
	if actualValue, ok := dataMap[key]; !ok {
		t.Errorf("Generated json should contain key %s", key)
	} else {
		if labels, ok := actualValue.([]interface{}); ok {
			if labels[0] != expectedValue {
				t.Errorf("Array with key %s should have size 1 / entry should have value '%s' but was '%s'", key, expectedValue, actualValue)
			}
		} else {
			t.Errorf("Value for key %s was not of type []string", key)
		}
	}
}

func CheckContainsKeyWithMapValue(dataMap map[string]interface{}, key string, internalKey string, expectedInternalValue string, t *testing.T) {
	if actualValue, ok := dataMap[key]; !ok {
		t.Errorf("Generated json should contain key %s", key)
	} else {
		if internalMap, ok := actualValue.(map[string]interface{}); !ok {
			t.Errorf("Value of key %s should be of type map[string]interface but wasn't", key)
		} else {
			if internalValue, ok := internalMap[internalKey]; !ok {
				t.Errorf("Value of key %s is a map but doesn't contain key %s", key, internalKey)
			} else if internalValue != expectedInternalValue {
				t.Errorf("Value of key %s is a map, but value of key %s should be %s but was %s", key, internalKey, expectedInternalValue, internalValue)
			}
		}
	}

}

func CheckContainsKeyWithBoolValue(dataMap map[string]interface{}, key string, expectedValue bool, t *testing.T) {
	if actualValue, ok := dataMap[key]; !ok {
		t.Errorf("Generated json should contain key %s", key)
	} else if actualValue != expectedValue {
		t.Errorf("Value of key %s should be '%t' but was '%t'", key, expectedValue, actualValue)
	}
}

func CheckContainsKeyWithFloat64Value(dataMap map[string]interface{}, key string, expectedValue float64, t *testing.T) {
	if actualValue, ok := dataMap[key]; !ok {
		t.Errorf("Generated json should contain key %s", key)
	} else if actualValue != expectedValue {
		t.Errorf("Value of key %s should be '%f' but was '%f'", key, expectedValue, actualValue)
	}
}

func CheckContainsKeyWithTimeValue(dataMap map[string]interface{}, key string, expectedValue time.Time, layout string, t *testing.T) {
	expectedTime := expectedValue.Format(layout)

	if actualValue, ok := dataMap[key]; !ok {
		t.Errorf("Generated json should contain key %s", key)
	} else if actualValue != expectedTime {
		t.Errorf("Value of key %s should be '%v' but was '%v'", key, expectedValue, actualValue)
	}
}
