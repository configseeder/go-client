package logic

import (
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/model"
	"testing"
)

var FALSE = false

func TestUniqueKeyConfigurationChangeDetector_GetChangedValuesForUniqueKeysOldValuesNil(t *testing.T) {

	var configValuesOld []model.ConfigValueDto = nil
	var configValuesNew []model.ConfigValueDto

	configuration := config.ClientConfiguration{
		FullForNonFiltered: &FALSE,
	}

	detector := UniqueKeyConfigurationChangeDetector{}
	actual, err := detector.GetChangedValues(configValuesNew, configValuesOld, &configuration)

	if err != nil {
		t.Fatalf("GetChangedValues() shouldn't return error")
	}
	if actual == nil {
		t.Error("slice must not be nil")
	}

	if len(actual) != 0 {
		t.Error("slice must be empty")
	}
}

func TestUniqueKeyConfigurationChangeDetector_GetChangedValuesForUniqueKeysOldValuesNilFirstRun(t *testing.T) {

	var configValuesOld []model.ConfigValueDto = nil
	var configValuesNew []model.ConfigValueDto

	key := "key"
	value := "value"
	var lastUpdateInMilliseconds int64 = 1

	configValuesNew = append(configValuesNew, model.ConfigValueDto{
		Key: key,
		LastUpdateInMilliseconds: lastUpdateInMilliseconds,
		Value: value,
	})

	configuration := config.ClientConfiguration{
		FullForNonFiltered: &FALSE,
	}

	detector := UniqueKeyConfigurationChangeDetector{}
	actual, err := detector.GetChangedValues(configValuesNew, configValuesOld, &configuration)

	if err != nil {
		t.Fatalf("GetChangedValues() shouldn't return error")
	}
	if actual == nil {
		t.Error("slice must not be nil")
	}

	if len(actual) != 1 {
		t.Error("slice must contain exactly one value")
	}

	checkAddedConfigValue(actual, key, value, lastUpdateInMilliseconds, t)
}

func TestUniqueKeyConfigurationChangeDetector_GetChangedValuesForUniqueKeysOldValuesNilFirstRunMultipleValues(t *testing.T) {

	key1 := "key1"
	value1 := "value1"
	var lastUpdateInMilliseconds1 int64 = 1

	configValue1 := model.ConfigValueDto{
		Key: key1,
		LastUpdateInMilliseconds: lastUpdateInMilliseconds1,
		Value: value1,
	}

	key2 := "key2"
	value2 := "value2"
	var lastUpdateInMilliseconds2 int64 = 2

	configValue2 := model.ConfigValueDto{
		Key: key2,
		LastUpdateInMilliseconds: lastUpdateInMilliseconds2,
		Value: value2,
	}

	var configValuesOld []model.ConfigValueDto = nil
	var configValuesNew = []model.ConfigValueDto{
		configValue1, configValue2,
	}

	configuration := config.ClientConfiguration{
		FullForNonFiltered: &FALSE,
	}

	detector := UniqueKeyConfigurationChangeDetector{}
	actual, err := detector.GetChangedValues(configValuesNew, configValuesOld, &configuration)

	if err != nil {
		t.Fatalf("GetChangedValues() shouldn't return error")
	}
	if actual == nil {
		t.Error("slice must not be nil")
	}

	if len(actual) != 2 {
		t.Error("slice must contain exactly two value")
	}

	checkAddedConfigValue(actual, key1, value1, lastUpdateInMilliseconds1, t)
	checkAddedConfigValue(actual, key2, value2, lastUpdateInMilliseconds2, t)
}

func TestUniqueKeyConfigurationChangeDetector_GetChangedValuesForUniqueKeysNoChanges(t *testing.T) {

	key := "key"
	value := "value"
	var lastUpdateInMilliseconds int64 = 1

	var configValuesOld = []model.ConfigValueDto{
		{
			Key: key,
			LastUpdateInMilliseconds: lastUpdateInMilliseconds,
			Value: value,
		},
	}
	var configValuesNew = []model.ConfigValueDto{
		{
			Key: key,
			LastUpdateInMilliseconds: lastUpdateInMilliseconds,
			Value: value,
		},
	}

	configuration := config.ClientConfiguration{
		FullForNonFiltered: &FALSE,
	}

	detector := UniqueKeyConfigurationChangeDetector{}
	actual, err := detector.GetChangedValues(configValuesNew, configValuesOld, &configuration)

	if err != nil {
		t.Fatalf("GetChangedValues() shouldn't return error")
	}
	if actual == nil {
		t.Error("map must not be nil")
	}

	if len(actual) != 0 {
		t.Error("map must be empty")
	}
}

func TestUniqueKeyConfigurationChangeDetector_GetChangedValuesForUniqueKeysUpdatedKey(t *testing.T) {

	key := "key"
	value := "value"
	modifiedValue := "modifiedValue"
	var lastUpdateInMilliseconds int64 = 1
	var modifiedLastUpdate int64 = 11

	var configValuesOld = []model.ConfigValueDto{
		{
			Key: key,
			LastUpdateInMilliseconds: lastUpdateInMilliseconds,
			Value: value,
		},
	}
	var configValuesNew = []model.ConfigValueDto{
		{
			Key: key,
			LastUpdateInMilliseconds: modifiedLastUpdate,
			Value: modifiedValue,
		},
	}

	configuration := config.ClientConfiguration{
		FullForNonFiltered: &FALSE,
	}

	detector := UniqueKeyConfigurationChangeDetector{}
	actual, err := detector.GetChangedValues(configValuesNew, configValuesOld, &configuration)

	if err != nil {
		t.Fatalf("GetChangedValues() shouldn't return error")
	}
	if actual == nil {
		t.Error("map must not be nil")
	}

	if len(actual) != 1 {
		t.Error("map must contain exactly one value")
	}

	checkModifiedConfigValue(actual, key, value, lastUpdateInMilliseconds, modifiedValue, modifiedLastUpdate, t)
}

func checkModifiedConfigValue(actual []model.ConfigValueChange, key string, value string, lastUpdateInMilliseconds int64,
	modifiedValue string, modifiedLastUpdateInMilliseconds int64, t *testing.T) {

	changedValue := getChangedValueByKey(actual, key)

	if changedValue == nil {
		t.Errorf("Unable to get ConfigValueChange identified by key %s", key)
		return
	}

	if changedValue.OldValue == nil {
		t.Errorf("key %s: OldValue must not be nil for value change", key)
	}
	if key != changedValue.OldValue.Key {
		t.Errorf("key %s: key must match but was %s", key, changedValue.OldValue.Key)
	}
	if value != changedValue.OldValue.Value {
		t.Errorf("Value %s: value must match but was %s", value, changedValue.OldValue.Value)
	}
	if lastUpdateInMilliseconds != changedValue.OldValue.LastUpdateInMilliseconds {
		t.Errorf("lastupdate %d: lastupdate must match but was %d", lastUpdateInMilliseconds, changedValue.OldValue.LastUpdateInMilliseconds)
	}
	if changedValue.NewValue == nil {
		t.Errorf("key %s: NewValue must not be nil for value change", key)
	}
	if key != changedValue.NewValue.Key {
		t.Errorf("key %s: key must match but was %s", key, changedValue.NewValue.Key)
	}
	if modifiedValue != changedValue.NewValue.Value {
		t.Errorf("Value %s: value must match but was %s", modifiedValue, changedValue.NewValue.Value)
	}
	if modifiedLastUpdateInMilliseconds != changedValue.NewValue.LastUpdateInMilliseconds {
		t.Errorf("lastupdate %d: lastupdate must match but was %d", modifiedLastUpdateInMilliseconds, changedValue.NewValue.LastUpdateInMilliseconds)
	}
}

func TestGetChangedValuesUpdateMultipleKeys(t *testing.T) {
	key1 := "key1"
	value1 := "value1"
	value1m := "value1m"
	var lastUpdate1 int64 = 1
	var lastUpdate1m int64 = 11

	key2 := "key2"
	value2 := "value2"
	value2m := "value2m"
	var lastUpdate2 int64 = 2
	var lastUpdate2m int64 = 22

	var configValuesOld = []model.ConfigValueDto{
		{
			Key: key1,
			LastUpdateInMilliseconds: lastUpdate1,
			Value: value1,
		},
		{
			Key: key2,
			LastUpdateInMilliseconds: lastUpdate2,
			Value: value2,
		},
	}
	var configValuesNew = []model.ConfigValueDto{
		{
			Key: key1,
			LastUpdateInMilliseconds: lastUpdate1m,
			Value: value1m,
		},
		{
			Key: key2,
			LastUpdateInMilliseconds: lastUpdate2m,
			Value: value2m,
		},
	}

	configuration := config.ClientConfiguration{
		FullForNonFiltered: &FALSE,
	}

	detector := UniqueKeyConfigurationChangeDetector{}
	actual, err := detector.GetChangedValues(configValuesNew, configValuesOld, &configuration)

	if err != nil {
		t.Fatalf("GetChangedValues() shouldn't return error")
	}
	if actual == nil {
		t.Error("map must not be nil")
	}

	if len(actual) != 2 {
		t.Error("map must contain exactly two values")
	}

	checkModifiedConfigValue(actual, key1, value1, lastUpdate1, value1m, lastUpdate1m, t)
	checkModifiedConfigValue(actual, key2, value2, lastUpdate2, value2m, lastUpdate2m, t)
}

func TestUniqueKeyConfigurationChangeDetector_GetChangedValuesForUniqueKeysRemovedKey(t *testing.T) {

	key := "key"
	value := "value"
	var lastUpdate int64 = 1

	removedKey := "removedKey"
	removedValue := "removedValue"
	var removedLastUpdate int64 = 2

	var configValuesOld = []model.ConfigValueDto{
		{
			Key: key,
			LastUpdateInMilliseconds: lastUpdate,
			Value: value,
		},
		{
			Key: removedKey,
			LastUpdateInMilliseconds: removedLastUpdate,
			Value: removedValue,
		},
	}
	var configValuesNew = []model.ConfigValueDto{
		{
			Key: key,
			LastUpdateInMilliseconds: lastUpdate,
			Value: value,
		},
	}

	configuration := config.ClientConfiguration{
		FullForNonFiltered: &FALSE,
	}

	detector := UniqueKeyConfigurationChangeDetector{}
	actual, err := detector.GetChangedValues(configValuesNew, configValuesOld, &configuration)

	if err != nil {
		t.Fatalf("GetChangedValues() shouldn't return error")
	}
	if actual == nil {
		t.Error("map must not be nil")
	}

	if len(actual) != 1 {
		t.Error("map must contain exactly one value")
	}

	checkRemovedConfigValue(actual, removedKey, removedValue, removedLastUpdate, t)
}

func TestUniqueKeyConfigurationChangeDetector_GetChangedValuesForUniqueKeysRemovedMultipleKeys(t *testing.T) {

	key := "key"
	value := "value"
	var lastUpdate int64 = 1

	removedKey1 := "removedKey1"
	removedValue1 := "removedValue1"
	var removedLastUpdate1 int64 = 11

	removedKey2 := "removedKey2"
	removedValue2 := "removedValue2"
	var removedLastUpdate2 int64 = 12

	var configValuesOld = []model.ConfigValueDto{
		{
			Key: key,
			LastUpdateInMilliseconds: lastUpdate,
			Value: value,
		},
		{
			Key: removedKey1,
			LastUpdateInMilliseconds: removedLastUpdate1,
			Value: removedValue1,
		},
		{
			Key: removedKey2,
			LastUpdateInMilliseconds: removedLastUpdate2,
			Value: removedValue2,
		},
	}
	var configValuesNew = []model.ConfigValueDto{
		{
			Key: key,
			LastUpdateInMilliseconds: lastUpdate,
			Value: value,
		},
	}

	configuration := config.ClientConfiguration{
		FullForNonFiltered: &FALSE,
	}

	detector := UniqueKeyConfigurationChangeDetector{}
	actual, err := detector.GetChangedValues(configValuesNew, configValuesOld, &configuration)

	if err != nil {
		t.Fatalf("GetChangedValues() shouldn't return error")
	}
	if actual == nil {
		t.Error("map must not be nil")
	}

	if len(actual) != 2 {
		t.Error("map must contain exactly two values")
	}

	checkRemovedConfigValue(actual, removedKey1, removedValue1, removedLastUpdate1, t)
	checkRemovedConfigValue(actual, removedKey2, removedValue2, removedLastUpdate2, t)
}

func checkAddedConfigValue(changedValues []model.ConfigValueChange, key string, value string, lastUpdateInMilliseconds int64, t *testing.T) {
	changedValue := getChangedValueByKey(changedValues, key)

	if changedValue == nil {
		t.Errorf("Unable to get ConfigValueChange identified by key %s", key)
		return
	}

	if changedValue.OldValue != nil {
		t.Errorf("key %s: OldValue must be nil for new key", key)
	}
	if changedValue.NewValue == nil {
		t.Errorf("key %s: NewValue must not be nil for new key", key)
	}
	if changedValue.NewValue.Key != key {
		t.Errorf("key %s: key must match but was %s", key, changedValue.NewValue.Key)
	}
	if value != changedValue.NewValue.Value {
		t.Errorf("Value %s: value must match but was %s", value, changedValue.NewValue.Value)
	}
	if lastUpdateInMilliseconds != changedValue.NewValue.LastUpdateInMilliseconds {
		t.Errorf("lastupdate %d: lastupdate must match but was %d", lastUpdateInMilliseconds, changedValue.NewValue.LastUpdateInMilliseconds)
	}
}

func getChangedValueByKey(changedValues []model.ConfigValueChange, key string) *model.ConfigValueChange {
	for _, value := range changedValues {
		if value.NewValue != nil && value.NewValue.Key == key ||
			value.OldValue != nil && value.OldValue.Key == key {
			return &value
		}
	}

	return nil
}

func checkRemovedConfigValue(changedValues []model.ConfigValueChange, key string, removedValue string, removedLastUpdate int64, t *testing.T) {
	changedValue := getChangedValueByKey(changedValues, key)

	if changedValue == nil {
		t.Errorf("Unable to get ConfigValueChange identified by key %s", key)
		return
	}

	if changedValue.NewValue != nil {
		t.Errorf("key %s: NewValue must be nil for removed key", key)
	}
	if changedValue.OldValue == nil {
		t.Errorf("key %s: OldValue must not be nil for removed key", key)
	}
	if key != changedValue.OldValue.Key {
		t.Errorf("key %s: key must match but was %s", key, changedValue.OldValue.Key)
	}
	if removedValue != changedValue.OldValue.Value {
		t.Errorf("Value %s: value must match but was %s", removedValue, changedValue.OldValue.Value)
	}
	if removedLastUpdate != changedValue.OldValue.LastUpdateInMilliseconds {
		t.Errorf("lastupdate %d: lastupdate must match but was %d", removedLastUpdate, changedValue.OldValue.LastUpdateInMilliseconds)
	}
}
