package logic

import (
	"errors"
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/model"
)

type ConfigurationChangeDetector interface {
	GetChangedValues(configValuesNew []model.ConfigValueDto, configValuesOld []model.ConfigValueDto, configuration *config.ClientConfiguration) ([]model.ConfigValueChange, error)
}

type UniqueKeyConfigurationChangeDetector struct{}

/*
 * Get all the differneces between new and old configvalues. This method assumes, that all the key's are unique.
 * (fullForNonFiltered set to false)
 */
func (d *UniqueKeyConfigurationChangeDetector) GetChangedValues(configValuesNew []model.ConfigValueDto,
	configValuesOld []model.ConfigValueDto, configuration *config.ClientConfiguration) ([]model.ConfigValueChange, error) {

	if configuration != nil && configuration.FullForNonFiltered != nil && *configuration.FullForNonFiltered == true {
		return nil, errors.New("retrieving changedValues in mode fullForNonFiltered=true not supported")
	}

	var changes = []model.ConfigValueChange{}

	// transform old values to a map
	var old = make(map[string]model.ConfigValueDto)
	for _, configValue := range configValuesOld {
		old[configValue.Key] = configValue
	}

	// Add changed and new keys to update structure
	var addedKeys = make(map[string]bool)
	for index, _ := range configValuesNew {
		newValue := configValuesNew[index]
		key := newValue.Key
		addedKeys[key] = true
		oldValue, ok := old[key]

		if !ok {
			changes = append(changes, model.ConfigValueChange{
				OldValue: nil,
				NewValue: &newValue,
			})
		} else if newValue.LastUpdateInMilliseconds != oldValue.LastUpdateInMilliseconds {
			// Value was modified
			changes = append(changes, model.ConfigValueChange{
				OldValue: &oldValue,
				NewValue: &newValue,
			})
		} else if newValue.Value != oldValue.Value {
			// New value valid (probably because of validFrom/validTo)
			changes = append(changes, model.ConfigValueChange{
				OldValue: &oldValue,
				NewValue: &newValue,
			})
		}
	}

	// Add deleted keys to update structure
	for oldKey, _ := range old {
		oldValue := old[oldKey] // necessary because later a pointer to this value is used. otherwise, data is overwritten
		_, ok := addedKeys[oldKey]

		if !ok {
			changes = append(changes, model.ConfigValueChange{
				OldValue: &oldValue,
				NewValue: nil,
			})
		}
	}
	return changes, nil
}
