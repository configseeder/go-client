/*
 * Copyright (c) 2024 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package logic

import (
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/model"
)

// CreateRequestConfiguration creates the request object for getting properties from the config seeder based on the client configuration.
func CreateRequestConfiguration(clientConfiguration config.ClientConfiguration) model.ConfigurationRequestDto {
	var configurationGroups []model.ConfigurationEntryRequestDto

	for _, configuration := range clientConfiguration.Configurations {
		configurationGroups = append(configurationGroups, CreateConfigurationEntryRequestDto(configuration))
	}

	return CreateConfigurationRequestDto(clientConfiguration, configurationGroups)
}

func CreateConfigurationEntryRequestDto(configuration config.GroupConfiguration) model.ConfigurationEntryRequestDto {
	var configurationGroupKey *string = nil
	if len(configuration.GroupKey) > 0 {
		configurationGroupKey = &configuration.GroupKey
	}
	var configurationGroupId *string = nil
	if len(configuration.ConfigurationGroupId) > 0 {
		configurationGroupId = &configuration.ConfigurationGroupId
	}

	configurationGroup := model.ConfigurationEntryRequestDto{
		ConfigurationGroupId:            configurationGroupId,
		ConfigurationGroupKey:           configurationGroupKey,
		ConfigurationGroupVersionNumber: configuration.Version,
		SelectionMode:                   configuration.SelectionMode,
	}
	return configurationGroup
}

func CreateConfigurationRequestDto(clientConfiguration config.ClientConfiguration,
	configurationGroups []model.ConfigurationEntryRequestDto) model.ConfigurationRequestDto {

	fullForNonFiltered := false
	if clientConfiguration.FullForNonFiltered != nil {
		fullForNonFiltered = *clientConfiguration.FullForNonFiltered
	}

	request := model.ConfigurationRequestDto{
		Configurations:     configurationGroups,
		DateTime:           clientConfiguration.DateTime,
		EnvironmentKey:     clientConfiguration.EnvironmentKey,
		FullForNonFiltered: fullForNonFiltered,
		TenantKey:          clientConfiguration.TenantKey,
	}

	if clientConfiguration.Context != nil {
		request.Context = clientConfiguration.Context
	}
	if clientConfiguration.Labels != nil {
		request.Labels = clientConfiguration.Labels
	}
	if clientConfiguration.Version != nil {
		request.Version = clientConfiguration.Version
	}
	return request
}
