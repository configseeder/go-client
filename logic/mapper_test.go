package logic

import (
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
	"reflect"
	"testing"
	"time"
)

func TestCreateRequestConfigurationNoConfigurations(t *testing.T) {
	clientConfiguration := config.ClientConfiguration{}

	actual := CreateRequestConfiguration(clientConfiguration)

	if actual.Configurations != nil {
		t.Error("Configurations must be nil")
	}
}

func TestCreateRequestConfiguration(t *testing.T) {
	clientConfiguration := config.ClientConfiguration{
		Configurations: []config.GroupConfiguration{
			{},
		},
	}

	actual := CreateRequestConfiguration(clientConfiguration)

	if actual.Configurations == nil {
		t.Error("Configurations must not be nil")
	}

	if len(actual.Configurations) != 1 {
		t.Error("There must be exactly one entry in Configurations")
	}
}

func TestCreateConfigurationEntryRequestDto(t *testing.T) {
	configurationGroupId := "configurationGroupId"
	configurationGroupKey := "configurationGroupKey"
	selectionMode := model.SelectionModeLatest
	version := "version"

	configuration := config.GroupConfiguration{
		GroupKey:             configurationGroupKey,
		ConfigurationGroupId: configurationGroupId,
		SelectionMode:        &selectionMode,
		Version:              &version,
	}

	actual := CreateConfigurationEntryRequestDto(configuration)

	if actual.ConfigurationGroupKey == nil {
		t.Error("ConfigurationGroupKey should not be nil")
	} else if *actual.ConfigurationGroupKey != configurationGroupKey {
		t.Errorf("ConfigurationGroupKey: Expected '%s', Given: '%s'", configurationGroupKey, *actual.ConfigurationGroupKey)
	}
	if actual.ConfigurationGroupId == nil {
		t.Error("ConfigurationGroupId should not be nil")
	} else if *actual.ConfigurationGroupId != configurationGroupId {
		t.Errorf("ConfigurationGroupId: Expected '%s', Given: '%s'", configurationGroupId, *actual.ConfigurationGroupId)
	}
	if actual.SelectionMode != &selectionMode {
		t.Errorf("SelectionMode: Expected '%s', Given: '%s'", selectionMode, getSelectionModeValue(actual.SelectionMode))
	}
	if actual.ConfigurationGroupVersionNumber != &version {
		t.Errorf("ConfigurationGroupVersionNumber: Expected '%s', Given: '%s'", version, getStringValue(actual.ConfigurationGroupVersionNumber))
	}
}

func TestCreateConfigurationEntryRequestDtoEmpty(t *testing.T) {
	var configuration config.GroupConfiguration

	actual := CreateConfigurationEntryRequestDto(configuration)

	if actual.SelectionMode != nil {
		t.Errorf("SelectionMode: Expected '<nil>', Given: '%s'", getSelectionModeValue(actual.SelectionMode))
	}
	if actual.ConfigurationGroupVersionNumber != nil {
		t.Errorf("ConfigurationGroupVersionNumber: Expected '<nil>', Given: '%s'", getStringValue(actual.ConfigurationGroupVersionNumber))
	}
	if actual.ConfigurationGroupKey != nil {
		t.Errorf("ConfigurationGroupKey: Expected '<nil>', Given: '%s'", *actual.ConfigurationGroupKey)
	}
}

func TestCreateConfigurationRequestDtoEmpty(t *testing.T) {
	clientConfiguration := config.ClientConfiguration{}

	actual := CreateConfigurationRequestDto(clientConfiguration, nil)

	if actual.Context != nil {
		t.Errorf("Context: Expected '<nil>', Given: '%s'", getStringValue(actual.Context))
	}
	if actual.DateTime != nil {
		t.Errorf("DateTime: Expected '<nil>', Given: '%s'", actual.DateTime.Format(time.RFC3339))
	}
	if actual.EnvironmentKey != nil {
		t.Errorf("TenantKey: Expected '<nil>', Given: '%s'", *actual.EnvironmentKey)
	}
	if actual.FullForNonFiltered != false {
		t.Errorf("FullForNonFiltered: Expected 'false', Given: '%t'", actual.FullForNonFiltered)
	}
	if actual.Labels != nil {
		t.Errorf("Labels: Expected '<nil>', Given: '%v'", actual.Labels)
	}
	if actual.TenantKey != "" {
		t.Errorf("TenantKey: Expected '', Given: '%s'", actual.TenantKey)
	}
	if actual.Version != nil {
		t.Errorf("Version: Expected '<nil>', Given: '%v'", getStringValue(actual.Version))
	}
	if actual.Configurations != nil {
		t.Error("Configurations on created ConfigurationRequestDto should be nil")
	}
}

func TestCreateConfigurationRequestDto(t *testing.T) {
	now := time.Now()
	context := "context"
	environment := "environment"
	fullForNonFiltered := true
	tenant := "tenant"
	version := "version"
	labels := []string{"label1", "label2"}

	clientConfiguration := config.ClientConfiguration{
		Context:            &context,
		DateTime:           &now,
		EnvironmentKey:     &environment,
		FullForNonFiltered: &fullForNonFiltered,
		Labels:             labels,
		TenantKey:          tenant,
		Version:            &version,
	}

	configurationGroups := []model.ConfigurationEntryRequestDto{
		{},
	}

	actual := CreateConfigurationRequestDto(clientConfiguration, configurationGroups)

	if *actual.Context != context {
		t.Errorf("Context: Expected '%s', Given: '%s'", context, getStringValue(actual.Context))
	}
	if util.TimeToFormattetTimeWithZone(actual.DateTime) == util.TimeToFormattetTimeWithZone(&now) {
		t.Errorf("DateTime: Expected '%s', Given: '%s'", now, actual.DateTime)
	}
	if *actual.EnvironmentKey != environment {
		t.Errorf("TenantKey: Expected '%s', Given: '%s'", environment, *actual.EnvironmentKey)
	}
	if actual.FullForNonFiltered != fullForNonFiltered {
		t.Errorf("FullForNonFiltered: Expected '%t', Given: '%t'", fullForNonFiltered, actual.FullForNonFiltered)
	}
	if !reflect.DeepEqual(actual.Labels, labels) {
		t.Errorf("Labels: Expected '%v', Given: '%v'", labels, actual.Labels)
	}
	if actual.TenantKey != tenant {
		t.Errorf("TenantKey: Expected '%s', Given: '%s'", tenant, actual.TenantKey)
	}
	if *actual.Version != version {
		t.Errorf("Version: Expected '%s', Given: '%v'", version, getStringValue(actual.Version))
	}
	if !reflect.DeepEqual(actual.Configurations, configurationGroups) {
		t.Error("Configurations on created ConfigurationRequestDto must be equal to defined configurationGroups Array")
	}
}

func getStringValue(value *string) string {
	if value == nil {
		return "<nil>"
	}
	return *value
}

func getSelectionModeValue(value *model.VersionedConfigurationGroupSelectionMode) string {
	if value == nil {
		return "<nil>"
	}
	return string(*value)
}
