package model

type WellKnown struct {
	Urls    map[string]string `json:"urls,omitempty"`
	Release map[string]string `json:"release,omitempty"`
}

func (w *WellKnown) GetUrl(key string) (string, bool) {
	value, ok := w.Urls[key]
	if ok && len(value) == 0 {
		return "", false
	}
	return value, ok
}

func (w *WellKnown) GetVersion() (string, bool) {
	value, ok := w.Release["version"]
	if ok && len(value) == 0 {
		return "", false
	}
	return value, ok
}
