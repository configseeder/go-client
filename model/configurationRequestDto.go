/*
 * Copyright (c) 2024 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package model

import (
	"encoding/json"
	"gitlab.com/configseeder/go-client/util"
	"time"
)

type ConfigurationRequestDto struct {
	TenantKey          string     `json:"tenantKey,omitempty"`
	EnvironmentKey     *string    `json:"environmentKey,omitempty"`
	FullForNonFiltered bool       `json:"fullForNonFiltered"`
	Context            *string    `json:"context,omitempty"`
	Labels             []string   `json:"labels,omitempty"`
	Version            *string    `json:"version,omitempty"`
	DateTime           *time.Time `json:"dateTime,omitempty"` // overridden by MarshalJSON

	Configurations []ConfigurationEntryRequestDto `json:"configurations"`
}

func (request *ConfigurationRequestDto) MarshalJSON() ([]byte, error) {
	type Alias ConfigurationRequestDto
	return json.Marshal(&struct {
		DateTime *string `json:"dateTime,omitempty"`
		*Alias
	}{
		DateTime: util.TimeToFormattetTimeWithZone(request.DateTime),
		Alias:    (*Alias)(request),
	})
}

func (request *ConfigurationRequestDto) UnmarshalJSON(data []byte) error {
	type Alias ConfigurationRequestDto
	aux := &struct {
		DateTime *string `json:"dateTime,omitempty"`
		*Alias
	}{
		Alias: (*Alias)(request),
	}

	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	dateTime, err := util.FormattetTimeWithZoneToTime(aux.DateTime)
	if err != nil {
		return err
	}

	request.DateTime = dateTime

	return nil
}
