package model

type ConfigurationEntryRequestDto struct {
	ConfigurationGroupId            *string                                   `json:"configurationGroupId,omitempty"`
	ConfigurationGroupKey           *string                                   `json:"configurationGroupKey,omitempty"`
	ConfigurationGroupVersionNumber *string                                   `json:"configurationGroupVersionNumber,omitempty"`
	SelectionMode                   *VersionedConfigurationGroupSelectionMode `json:"selectionMode,omitempty"`
}

type VersionedConfigurationGroupSelectionMode string

const (
	// Latest versioned configuration group.
	SelectionModeLatest VersionedConfigurationGroupSelectionMode = "LATEST"

	//Latest released versioned configuration group.
	SelectionModeLatestReleased VersionedConfigurationGroupSelectionMode = "LATEST_RELEASED"

	// By exact version number.
	SelectionModeVersionNumber VersionedConfigurationGroupSelectionMode = "VERSION_NUMBER"
)
