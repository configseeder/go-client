package model

/*
 * This datastructure is used by the KeyUpdateListener and transports information about a changed configuration value.
 */
type ConfigValueChange struct {
	OldValue *ConfigValueDto
	NewValue *ConfigValueDto
}
