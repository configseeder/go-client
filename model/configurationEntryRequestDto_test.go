package model

import (
	"encoding/json"
	. "gitlab.com/configseeder/go-client/util_test"
	"testing"
)

const entryRequestAttributeCount = 4
const entryRequestAttributeCountNotOmitted = 0

func TestConfigurationEntryRequestDto_MarshalJSONEmptyObject(t *testing.T) {
	configValue := ConfigurationEntryRequestDto{}

	actual, err := json.Marshal(&configValue)

	if err != nil {
		t.Errorf("Marshaling empty Dto shouldn't result in error: %s", err)
	}

	var f interface{}
	err = json.Unmarshal(actual, &f)
	if err != nil {
		t.Errorf("Unmarshaling json shouldn't result in error: %s", err)
	}

	dataMap := f.(map[string]interface{})

	if len(dataMap) != entryRequestAttributeCountNotOmitted { // empty attributes are omitted
		t.Errorf("Object should have exactly %d attributes but had %d", entryRequestAttributeCountNotOmitted, len(dataMap))
	}
}

func TestConfigurationEntryRequestDto_MarshalJSON(t *testing.T) {
	groupKey := "groupKey"
	groupId := "groupId"
	versionNumber := "versionNumber"
	selectionMode := SelectionModeLatestReleased

	configValue := ConfigurationEntryRequestDto{
		ConfigurationGroupKey:           &groupKey,
		ConfigurationGroupId:            &groupId,
		ConfigurationGroupVersionNumber: &versionNumber,
		SelectionMode:                   &selectionMode,
	}

	actual, err := json.Marshal(&configValue)

	if err != nil {
		t.Errorf("Marshaling empty Dto shouldn't result in error: %s", err)
	}

	var f interface{}
	err = json.Unmarshal(actual, &f)
	if err != nil {
		t.Errorf("Unmarshaling json shouldn't result in error: %s", err)
	}

	dataMap := f.(map[string]interface{})

	if len(dataMap) != entryRequestAttributeCount {
		t.Errorf("Object should have exactly %d attributes", entryRequestAttributeCount)
	}

	CheckContainsKeyWithStringValue(dataMap, "configurationGroupKey", groupKey, t)
	CheckContainsKeyWithStringValue(dataMap, "configurationGroupId", groupId, t)
	CheckContainsKeyWithStringValue(dataMap, "configurationGroupVersionNumber", versionNumber, t)
	CheckContainsKeyWithStringValue(dataMap, "selectionMode", string(selectionMode), t)
}

func TestConfigurationEntryRequestDto_UnmarshalJSONEmptyJson(t *testing.T) {

	data := "{}"

	actual := &ConfigurationEntryRequestDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.ConfigurationGroupVersionNumber != nil {
		t.Error("Attribute ConfigurationGroupVersionNumber should be <nil>")
	}
	if actual.ConfigurationGroupKey != nil {
		t.Error("Attribute ConfigurationGroupKey should be nil")
	}
	if actual.SelectionMode != nil {
		t.Error("Attribute SelectionMode should be <nil>")
	}
}

func TestConfigurationEntryRequestDto_UnmarshalJSON(t *testing.T) {
	groupKey := "groupKey"
	groupId := "groupId"
	selectionMode := SelectionModeLatestReleased
	versionNumber := "versionNumber"

	data := "{\n" +
		"\"configurationGroupKey\":\"" + groupKey + "\",\n" +
		"\"configurationGroupId\":\"" + groupId + "\",\n" +
		"\"configurationGroupVersionNumber\":\"" + versionNumber + "\",\n" +
		"\"selectionMode\":\"" + string(selectionMode) + "\"\n" +
		"}"

	actual := &ConfigurationEntryRequestDto{}

	json.Unmarshal([]byte(data), actual)

	if GetStringValue(actual.ConfigurationGroupVersionNumber) != versionNumber {
		t.Errorf("Attribute ConfigurationGroupVersionNumber should be '%s' but was '%s'", versionNumber, GetStringValue(actual.ConfigurationGroupVersionNumber))
	}
	if actual.ConfigurationGroupKey == nil {
		t.Error("Attribute ConfigurationGroupKey should not be nil")
	} else if *actual.ConfigurationGroupKey != groupKey {
		t.Errorf("Attribute ConfigurationGroupKey should be '%s' but was '%s'", groupKey, *actual.ConfigurationGroupKey)
	}
	if actual.ConfigurationGroupId == nil {
		t.Error("Attribute ConfigurationGroupId should not be nil")
	} else if *actual.ConfigurationGroupId != groupId {
		t.Errorf("Attribute ConfigurationGroupId should be '%s' but was '%s'", groupId, *actual.ConfigurationGroupId)
	}
	if getStringValue(actual.SelectionMode) != string(selectionMode) {
		t.Errorf("Attribute SelectionMode should be '%s' but was '%s'", selectionMode, getStringValue(actual.SelectionMode))
	}
}

func getStringValue(selectionMode *VersionedConfigurationGroupSelectionMode) string {
	if selectionMode == nil {
		return "<nil>"
	}

	return string(*selectionMode)
}
