package model

import (
	"encoding/json"
	. "gitlab.com/configseeder/go-client/util_test"
	"testing"
	"time"
)

const requestAttributeCount = 7
const requestAttributeCountNotOmitted = 2

func TestConfigurationRequestDto_MarshalJSONEmptyObject(t *testing.T) {
	configValue := ConfigurationRequestDto{}

	actual, err := configValue.MarshalJSON()

	if err != nil {
		t.Errorf("Marshaling empty Dto shouldn't result in error: %s", err)
	}

	var f interface{}
	err = json.Unmarshal(actual, &f)
	if err != nil {
		t.Errorf("Unmarshaling json shouldn't result in error: %s", err)
	}

	dataMap := f.(map[string]interface{})

	if len(dataMap) != requestAttributeCountNotOmitted { // empty attributes are omitted
		t.Errorf("Object should have exactly %d attributes but had %d", requestAttributeCountNotOmitted, len(dataMap))
	}

	CheckContainsKeyWithNull(dataMap, "configurations", t)
	CheckContainsKeyWithBoolValue(dataMap, "fullForNonFiltered", false, t)
}

func TestConfigurationRequestDto_MarshalJSON(t *testing.T) {
	tenantKey := "tenantKey"
	environmentKey := "environmentKey"
	fullForNonFiltered := true
	context := "context"
	version := "version"
	dateTime, _ := time.Parse(time.RFC3339, "2018-01-01T08:01:02")

	configValue := ConfigurationRequestDto{
		Configurations:     []ConfigurationEntryRequestDto{},
		Context:            &context,
		DateTime:           &dateTime,
		EnvironmentKey:     &environmentKey,
		FullForNonFiltered: fullForNonFiltered,
		TenantKey:          tenantKey,
		Version:            &version,
	}

	actual, err := configValue.MarshalJSON()

	if err != nil {
		t.Errorf("Marshaling empty Dto shouldn't result in error: %s", err)
	}

	var f interface{}
	err = json.Unmarshal(actual, &f)
	if err != nil {
		t.Errorf("Unmarshaling json shouldn't result in error: %s", err)
	}

	dataMap := f.(map[string]interface{})

	if len(dataMap) != requestAttributeCount {
		t.Errorf("Object should have exactly %d attributes", requestAttributeCount)
	}

	CheckContainsKeyNotNull(dataMap, "configurations", t)
	CheckContainsKeyWithStringValue(dataMap, "context", context, t)
	CheckContainsKeyWithTimeValue(dataMap, "dateTime", dateTime, time.RFC3339, t)
	CheckContainsKeyWithStringValue(dataMap, "environmentKey", environmentKey, t)
	CheckContainsKeyWithStringValue(dataMap, "tenantKey", tenantKey, t)
	CheckContainsKeyWithStringValue(dataMap, "version", version, t)
}

func TestConfigurationRequestDto_UnmarshalJSONEmptyJson(t *testing.T) {

	data := "{}"

	actual := &ConfigurationRequestDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Configurations != nil {
		t.Error("Attribute Configurations should be <nil>")
	}
	if actual.Context != nil {
		t.Error("Attribute Context should be <nil>")
	}
	if actual.DateTime != nil {
		t.Error("Attribute DateTime should be <nil>")
	}
	if actual.EnvironmentKey != nil {
		t.Error("Attribute EnvironmentKey should be ''")
	}
	if actual.TenantKey != "" {
		t.Error("Attribute TenantKey should be ''")
	}
	if actual.Version != nil {
		t.Error("Attribute Version should be <nil>")
	}
}

func TestConfigurationRequestDto_UnmarshalJSON(t *testing.T) {
	context := "context"
	dateTime, _ := time.Parse(time.RFC3339, "2018-01-01T08:01:02")
	environmentKey := "environmentKey"
	tenantKey := "tenantKey"
	version := "version"

	data := "{\n" +
		"\"configurations\":[\n" +
		"    {\n" +
		"    }\n" +
		"],\n" +
		"\"context\":\"" + context + "\",\n" +
		"\"dateTime\":\"" + dateTime.Format(time.RFC3339) + "\",\n" +
		"\"environmentKey\":\"" + environmentKey + "\",\n" +
		"\"tenantKey\":\"" + tenantKey + "\",\n" +
		"\"version\":\"" + version + "\"\n" +
		"}"

	actual := &ConfigurationRequestDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Configurations == nil {
		t.Error("Attribute Configurations should not be <nil>")
	}
	if GetStringValue(actual.Context) != context {
		t.Errorf("Attribute Context should be '%s' but was '%s'", context, GetStringValue(actual.Context))
	}
	if *actual.DateTime != dateTime {
		t.Errorf("Attribute DateTime should be '%s' but was '%s'", dateTime, *actual.DateTime)
	}
	if *actual.EnvironmentKey != environmentKey {
		t.Errorf("Attribute EnvironmentKey should be '%s' but was '%s'", environmentKey, *actual.EnvironmentKey)
	}
	if actual.TenantKey != tenantKey {
		t.Errorf("Attribute LastUpdate should be '%s' but was '%s'", tenantKey, actual.TenantKey)
	}
	if GetStringValue(actual.Version) != version {
		t.Errorf("Attribute LastUpdateAsString should be '%s' but was '%s'", version, GetStringValue(actual.Version))
	}
}
