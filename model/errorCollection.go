package model

import "strings"

type ErrorCollection struct {
	message string
	errors  []error
}

func NewErrorCollection(message string) *ErrorCollection {
	error := ErrorCollection{
		message: message,
	}

	return &error
}

func (e *ErrorCollection) Error() string {

	builder := strings.Builder{}
	builder.WriteString(e.message)

	for _, err := range e.errors {
		builder.WriteString("\n\t")
		builder.WriteString(err.Error())
	}

	return builder.String()
}

func (e *ErrorCollection) Add(err error) {
	e.errors = append(e.errors, err)
}

func (e *ErrorCollection) HasError() bool {
	return len(e.errors) > 0
}
