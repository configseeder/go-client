package model

import (
	"encoding/json"
	"fmt"
	. "gitlab.com/configseeder/go-client/util_test"
	"strconv"
	"testing"
	"time"
)

const configValueAttributeCount = 16

func TestConfigValueDto_MarshalJSONEmptyObject(t *testing.T) {
	configValue := ConfigValueDto{}

	actual, err := configValue.MarshalJSON()

	if err != nil {
		t.Errorf("Marshaling empty Dto shouldn't result in error: %s", err)
	}

	var f interface{}
	err = json.Unmarshal(actual, &f)
	if err != nil {
		t.Errorf("Unmarshaling json shouldn't result in error: %s", err)
	}

	dataMap := f.(map[string]interface{})

	if len(dataMap) != configValueAttributeCount {
		t.Errorf("Object should have exactly %d attributes", configValueAttributeCount)
	}

	CheckContainsKeyWithStringValue(dataMap, "configurationValueId", "", t)
	CheckContainsKeyWithNull(dataMap, "context", t)
	CheckContainsKeyWithNull(dataMap, "environment", t)
	CheckContainsKeyWithNull(dataMap, "environmentId", t)
	CheckContainsKeyWithStringValue(dataMap, "key", "", t)
	CheckContainsKeyWithNull(dataMap, "labels", t)
	CheckContainsKeyWithFloat64Value(dataMap, "lastUpdateInMilliseconds", 0, t)
	CheckContainsKeyWithStringValue(dataMap, "lastUpdate", "", t)
	CheckContainsKeyWithBoolValue(dataMap, "secured", false, t)
	CheckContainsKeyWithStringValue(dataMap, "type", "", t)
	CheckContainsKeyWithNull(dataMap, "validFrom", t)
	CheckContainsKeyWithNull(dataMap, "validTo", t)
	CheckContainsKeyWithStringValue(dataMap, "value", "", t)
	CheckContainsKeyWithNull(dataMap, "versionFrom", t)
	CheckContainsKeyWithNull(dataMap, "versionTo", t)
	CheckContainsKeyWithNull(dataMap, "additionalParameters", t)
}

func TestConfigValueDto_MarshalJSON(t *testing.T) {
	configurationValueId := "configurationValueId"
	context := "context"
	environment := "environment"
	environmentId := "environmentId"
	key := "key"
	label := "label"
	var lastUpdateInMilliseconds int64 = 17
	lastUpdate := "lastUpdateAsString"
	secured := true
	valueType := ValueTypeString
	validTo, _ := time.Parse(time.RFC3339, "2018-01-01T08:01:02")
	validFrom, _ := time.Parse(time.RFC3339, "2018-12-31T23:50:09")
	value := "value"
	versionFrom := "versionFrom"
	versionTo := "versionTo"
	additionalParameters := map[string]string{
		"some": "parameter",
	}

	configValue := ConfigValueDto{
		ConfigurationValueId:     configurationValueId,
		Context:                  &context,
		Environment:              &environment,
		EnvironmentId:            &environmentId,
		Key:                      key,
		Labels:                   []string{label},
		LastUpdateInMilliseconds: lastUpdateInMilliseconds,
		LastUpdate:               lastUpdate,
		Secured:                  secured,
		Type:                     valueType,
		ValidFrom:                &validFrom,
		ValidTo:                  &validTo,
		Value:                    value,
		VersionFrom:              &versionFrom,
		VersionTo:                &versionTo,
		AdditionalParameters:     additionalParameters,
	}

	actual, err := configValue.MarshalJSON()

	if err != nil {
		t.Errorf("Marshaling Dto shouldn't result in error: %s", err)
	}

	fmt.Println(string(actual))

	var f interface{}
	err = json.Unmarshal(actual, &f)
	if err != nil {
		t.Errorf("Unmarshaling json shouldn't result in error: %s", err)
	}

	dataMap := f.(map[string]interface{})

	if len(dataMap) != configValueAttributeCount {
		t.Errorf("Object should have exactly %d attributes", configValueAttributeCount)
	}

	CheckContainsKeyWithStringValue(dataMap, "configurationValueId", configurationValueId, t)
	CheckContainsKeyWithStringValue(dataMap, "context", context, t)
	CheckContainsKeyWithStringValue(dataMap, "environment", environment, t)
	CheckContainsKeyWithStringValue(dataMap, "environmentId", environmentId, t)
	CheckContainsKeyWithStringValue(dataMap, "key", key, t)
	CheckContainsKeyWithStringArrayValue(dataMap, "labels", label, t)
	CheckContainsKeyWithFloat64Value(dataMap, "lastUpdateInMilliseconds", float64(lastUpdateInMilliseconds), t)
	CheckContainsKeyWithStringValue(dataMap, "lastUpdate", lastUpdate, t)
	CheckContainsKeyWithBoolValue(dataMap, "secured", true, t)
	CheckContainsKeyWithStringValue(dataMap, "type", string(valueType), t)
	CheckContainsKeyWithTimeValue(dataMap, "validFrom", validFrom, time.RFC3339, t)
	CheckContainsKeyWithTimeValue(dataMap, "validTo", validTo, time.RFC3339, t)
	CheckContainsKeyWithStringValue(dataMap, "value", value, t)
	CheckContainsKeyWithStringValue(dataMap, "versionFrom", versionFrom, t)
	CheckContainsKeyWithStringValue(dataMap, "versionTo", versionTo, t)
	CheckContainsKeyWithMapValue(dataMap, "additionalParameters", "some", "parameter", t)
}

func TestConfigValueDto_UnmarshalJSONEmptyJson(t *testing.T) {

	data := "{}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.ConfigurationValueId != "" {
		t.Error("Attribute ConfigurationValueId should be ''")
	}
	if actual.Context != nil {
		t.Error("Attribute Context should be <nil>")
	}
	if actual.Environment != nil {
		t.Error("Attribute Environment should be <nil>")
	}
	if actual.EnvironmentId != nil {
		t.Error("Attribute EnvironmentId should be <nil>")
	}
	if actual.Key != "" {
		t.Error("Attribute Key should be ''")
	}
	if actual.LastUpdateInMilliseconds != 0 {
		t.Error("Attribute LastUpdateInMilliseconds should be 0")
	}
	if actual.LastUpdate != "" {
		t.Error("Attribute LastUpdate should be ''")
	}
	if actual.Secured != false {
		t.Error("Attribute Secured should be false")
	}
	if actual.Type != "" {
		t.Error("Attribute Type should be ''")
	}
	if actual.ValidFrom != nil {
		t.Error("Attribute ValidFrom should be <nil>")
	}
	if actual.ValidTo != nil {
		t.Error("Attribute ValidTo should be <nil>")
	}
	if actual.Value != "" {
		t.Error("Attribute Value should be ''")
	}
	if actual.VersionFrom != nil {
		t.Error("Attribute VersionFrom should be <nil>")
	}
	if actual.VersionTo != nil {
		t.Error("Attribute VersionTo should be <nil>")
	}
	if actual.AdditionalParameters != nil {
		t.Error("Attribute AdditionalParameters should be <nil>")
	}

}

func TestConfigValueDto_UnmarshalJSON(t *testing.T) {
	configurationValueId := "configurationValueId"
	context := "context"
	environment := "environment"
	environmentId := "environmentId"
	key := "key"
	var lastUpdateInMilliseconds int64 = 17
	lastUpdate := "lastUpdate"
	secured := true
	valueType := ValueTypeString
	validTo, _ := time.Parse(time.RFC3339, "2018-01-01T08:01:02")
	validFrom, _ := time.Parse(time.RFC3339, "2018-12-31T23:50:09")
	value := "value"
	versionFrom := "versionFrom"
	versionTo := "versionTo"
	additionalParameterKey := "some"
	additionalParameterValue := "parameter"

	data := "{\n" +
		"\"configurationValueId\":\"" + configurationValueId + "\",\n" +
		"\"context\":\"" + context + "\",\n" +
		"\"environment\":\"" + environment + "\",\n" +
		"\"environmentId\":\"" + environmentId + "\",\n" +
		"\"key\":\"" + key + "\",\n" +
		"\"lastUpdateInMilliseconds\":" + strconv.FormatInt(lastUpdateInMilliseconds, 10) + ",\n" +
		"\"lastUpdate\":\"" + lastUpdate + "\",\n" +
		"\"secured\": " + strconv.FormatBool(secured) + ",\n" +
		"\"type\":\"" + string(valueType) + "\",\n" +
		"\"validFrom\":\"" + validFrom.Format(time.RFC3339) + "\",\n" +
		"\"validTo\":\"" + validTo.Format(time.RFC3339) + "\",\n" +
		"\"value\":\"" + value + "\",\n" +
		"\"versionFrom\":\"" + versionFrom + "\",\n" +
		"\"versionTo\":\"" + versionTo + "\",\n" +
		"\"additionalParameters\":{\"" + additionalParameterKey + "\":\"" + additionalParameterValue + "\"}\n" +
		"}"

	fmt.Println(data)

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.ConfigurationValueId != configurationValueId {
		t.Errorf("Attribute ConfigurationValueId should be '%s' but was '%s'", configurationValueId, actual.ConfigurationValueId)
	}
	if GetStringValue(actual.Context) != context {
		t.Errorf("Attribute Context should be '%s' but was '%s'", context, GetStringValue(actual.Context))
	}
	if GetStringValue(actual.Environment) != environment {
		t.Errorf("Attribute Environment should be '%s' but was '%s'", environment, GetStringValue(actual.Environment))
	}
	if GetStringValue(actual.EnvironmentId) != environmentId {
		t.Errorf("Attribute EnvironmentId should be '%s' but was '%s'", environmentId, GetStringValue(actual.EnvironmentId))
	}
	if actual.Key != key {
		t.Errorf("Attribute Key should be '%s' but was '%s'", key, actual.Key)
	}
	if actual.LastUpdateInMilliseconds != int64(lastUpdateInMilliseconds) {
		t.Errorf("Attribute LastUpdate should be '%d' but was '%d'", lastUpdateInMilliseconds, actual.LastUpdateInMilliseconds)
	}
	if actual.LastUpdate != lastUpdate {
		t.Errorf("Attribute LastUpdateAsString should be '%s' but was '%s'", lastUpdate, actual.LastUpdate)
	}
	if actual.Secured != secured {
		t.Errorf("Attribute Secured should be '%t' but was '%t'", secured, actual.Secured)
	}
	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
	if *actual.ValidFrom != validFrom {
		t.Errorf("Attribute ValidFrom should be '%v' but was '%v'", validFrom, *actual.ValidFrom)
	}
	if *actual.ValidTo != validTo {
		t.Errorf("Attribute ValidTo should be '%v' but was '%v'", validTo, *actual.ValidTo)
	}
	if actual.Value != value {
		t.Errorf("Attribute Value should be '%s' but was '%s'", value, actual.Value)
	}
	if GetStringValue(actual.VersionFrom) != versionFrom {
		t.Errorf("Attribute VersionFrom should be '%s' but was '%s'", versionFrom, GetStringValue(actual.VersionFrom))
	}
	if GetStringValue(actual.VersionTo) != versionTo {
		t.Errorf("Attribute VersionTo should be '%s' but was '%s'", versionTo, GetStringValue(actual.VersionTo))
	}
	if actual.AdditionalParameters == nil {
		t.Errorf("AdditionalParameters shouldn't be nil but was")
	} else if actual.AdditionalParameters[additionalParameterKey] != additionalParameterValue {
		t.Errorf("Attribute '%s' in map '%s' should be '%s' but was '%s'", additionalParameterKey,
			"AdditionalParameters", additionalParameterValue, actual.AdditionalParameters[additionalParameterKey])
	}
}

func TestConfigValueDto_UnmarshalJSONBoolean(t *testing.T) {
	valueType := ValueTypeBoolean

	data := "{\n" +
		"\"type\":\"" + string(valueType) + "\"\n" +
		"}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
}

func TestConfigValueDto_UnmarshalJSONCertificate(t *testing.T) {
	valueType := ValueTypeCertificate

	data := "{\n" +
		"\"type\":\"" + string(valueType) + "\"\n" +
		"}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
}
func TestConfigValueDto_UnmarshalJSONConfigurationGroup(t *testing.T) {
	valueType := ValueTypeConfigurationGroup

	data := "{\n" +
		"\"type\":\"" + string(valueType) + "\"\n" +
		"}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
}

func TestConfigValueDto_UnmarshalJSONEnum(t *testing.T) {
	valueType := ValueTypeEnum

	data := "{\n" +
		"\"type\":\"" + string(valueType) + "\"\n" +
		"}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
}

func TestConfigValueDto_UnmarshalJSONInteger(t *testing.T) {
	valueType := ValueTypeInteger

	data := "{\n" +
		"\"type\":\"" + string(valueType) + "\"\n" +
		"}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
}

func TestConfigValueDto_UnmarshalJSONMultilineString(t *testing.T) {
	valueType := ValueTypeMultilineString

	data := "{\n" +
		"\"type\":\"" + string(valueType) + "\"\n" +
		"}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
}

func TestConfigValueDto_UnmarshalJSONNumber(t *testing.T) {
	valueType := ValueTypeNumber

	data := "{\n" +
		"\"type\":\"" + string(valueType) + "\"\n" +
		"}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
}

func TestConfigValueDto_UnmarshalJSONPrivateKey(t *testing.T) {
	valueType := ValueTypePrivateKey

	data := "{\n" +
		"\"type\":\"" + string(valueType) + "\"\n" +
		"}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
}

func TestConfigValueDto_UnmarshalJSONRegex(t *testing.T) {
	valueType := ValueTypeRegex

	data := "{\n" +
		"\"type\":\"" + string(valueType) + "\"\n" +
		"}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
}

func TestConfigValueDto_UnmarshalJSONString(t *testing.T) {
	valueType := ValueTypeString

	data := "{\n" +
		"\"type\":\"" + string(valueType) + "\"\n" +
		"}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
}

func TestConfigValueDto_UnmarshalJSONTemplate(t *testing.T) {
	valueType := ValueTypeTemplate

	data := "{\n" +
		"\"type\":\"" + string(valueType) + "\"\n" +
		"}"

	actual := &ConfigValueDto{}

	json.Unmarshal([]byte(data), actual)

	if actual.Type != valueType {
		t.Errorf("Attribute Type should be '%s' but was '%s'", valueType, actual.Type)
	}
}
