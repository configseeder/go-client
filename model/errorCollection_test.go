package model

import (
	"errors"
	"testing"
)

func TestErrorCollection_hasError(t *testing.T) {
	error := &ErrorCollection{}

	actual := error.HasError()
	if actual {
		t.Error("hasError() should return false")
	}

	error.Add(errors.New("e"))

	actual = error.HasError()
	if !actual {
		t.Error("hasError() should return true")
	}
}

func TestErrorCollection_Error(t *testing.T) {

	error := &ErrorCollection{
		message: "m",
	}

	error.Add(errors.New("e1"))
	error.Add(errors.New("e2"))

	actual := error.Error()

	expected := "m\n\te1\n\te2"
	if expected != actual {
		t.Errorf("Expected Error() to return '%s' but got '%s'", expected, actual)
	}
}
