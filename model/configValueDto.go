/*
 * Copyright (c) 2024 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package model

import (
	"encoding/json"
	"gitlab.com/configseeder/go-client/util"
	"time"
)

type ConfigValueDto struct {
	Key                      string                 `json:"key"`
	Value                    string                 `json:"value"`
	Type                     ConfigurationValueType `json:"type"`
	ConfigurationValueId     string                 `json:"configurationValueId"`
	Secured                  bool                   `json:"secured"`
	Environment              *string                `json:"environment"`
	EnvironmentId            *string                `json:"environmentId"`
	ValidFrom                *time.Time             `json:"validFrom"`
	ValidTo                  *time.Time             `json:"validTo"`
	VersionFrom              *string                `json:"versionFrom"`
	VersionTo                *string                `json:"versionTo"`
	Context                  *string                `json:"context"`
	Labels                   []string               `json:"labels"`
	LastUpdateInMilliseconds int64                  `json:"lastUpdateInMilliseconds"`
	LastUpdate               string                 `json:"lastUpdate"`
	AdditionalParameters     map[string]string      `json:"additionalParameters"`
}

type ConfigurationValueType string

const (
	ValueTypeBoolean            ConfigurationValueType = "BOOLEAN"
	ValueTypeCertificate        ConfigurationValueType = "CERTIFICATE"
	ValueTypeConfigurationGroup ConfigurationValueType = "CONFIGURATION_GROUP"
	ValueTypeEnum               ConfigurationValueType = "ENUM"
	ValueTypeInteger            ConfigurationValueType = "INTEGER"
	ValueTypeMultilineString    ConfigurationValueType = "MULTILINE_STRING"
	ValueTypeNumber             ConfigurationValueType = "NUMBER"
	ValueTypePrivateKey         ConfigurationValueType = "PRIVATE_KEY"
	ValueTypeRegex              ConfigurationValueType = "REGEX"
	ValueTypeString             ConfigurationValueType = "STRING"
	ValueTypeTemplate           ConfigurationValueType = "TEMPLATE"
)

func (value *ConfigValueDto) MarshalJSON() ([]byte, error) {
	type Alias ConfigValueDto
	return json.Marshal(&struct {
		ValidFrom *string `json:"validFrom"`
		ValidTo   *string `json:"validTo"`
		*Alias
	}{
		ValidFrom: util.TimeToFormattetTimeWithZone(value.ValidFrom),
		ValidTo:   util.TimeToFormattetTimeWithZone(value.ValidTo),
		Alias:     (*Alias)(value),
	})
}

func (value *ConfigValueDto) UnmarshalJSON(data []byte) error {
	type Alias ConfigValueDto
	aux := &struct {
		ValidFrom *string `json:"validFrom"`
		ValidTo   *string `json:"validTo"`
		*Alias
	}{
		Alias: (*Alias)(value),
	}

	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	validFrom, err := util.FormattetTimeWithZoneToTime(aux.ValidFrom)
	if err != nil {
		return err
	}

	validTo, err := util.FormattetTimeWithZoneToTime(aux.ValidTo)
	if err != nil {
		return err
	}

	value.ValidFrom = validFrom
	value.ValidTo = validTo

	return nil
}
