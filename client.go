/*
 * Copyright (c) 2024 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package configseeder

import (
	"encoding/json"
	"errors"
	"fmt"
	semver "github.com/Masterminds/semver/v3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/configseeder/go-client/backend"
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/logic"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
	"sync"
	"time"
)

const CONFIGSEEDER_VERSION_WITH_LABELING_SUPPORT = "2.25.0"

type ClientImplementation struct {
	configuration           *config.ClientConfiguration
	configurationRequestDto *model.ConfigurationRequestDto
	changeDetector          logic.ConfigurationChangeDetector

	configurationBackend backend.ConfigurationBackend
	WellknownBackend     backend.WellKnownBackend

	// set to true when the configuration is loaded for the first time
	valuesInitialized bool

	// timestamp of the last synchronisation / loading of configuration values in milliseconds
	lastSynchronization int64

	cachedConfigValues []model.ConfigValueDto

	// Used to ensure that no parallel data loading occures
	mutex sync.Mutex

	keyUpdateListeners []KeyUpdateListener

	wellknown model.WellKnown
}

func (c *ClientImplementation) InitFlags() {
	config.InitFlags()
}

func (c *ClientImplementation) Init(changeDetector logic.ConfigurationChangeDetector) error {
	if c.configuration != nil {
		log.Warnf("Client already initialized, no action required")
		return nil
	}

	clientConfiguration, err := config.GetConfiguration()
	if err != nil {
		message := fmt.Sprintf("Unable to initialize go client: %s", err)
		return errors.New(message)
	}

	return c.InitWith(clientConfiguration, nil, changeDetector)
}

func (c *ClientImplementation) InitWith(clientConfiguration *config.ClientConfiguration, userAgentInfo *util.UserAgentInfo,
	changeDetector logic.ConfigurationChangeDetector) error {
	if c.configuration != nil {
		log.Warnf("Client already initialized, no action required")
		return nil
	}

	if c.configurationBackend == nil {
		info, err := util.GetUserAgentInfo(userAgentInfo)
		if err != nil {
			return fmt.Errorf("unable to initialize client: %s", err)
		}

		c.configurationBackend = backend.InitConfiguration(*info)
	}

	if c.WellknownBackend == nil {
		info, err := util.GetUserAgentInfo(userAgentInfo)
		if err != nil {
			return fmt.Errorf("unable to initialize client: %s", err)
		}

		c.WellknownBackend = backend.InitWellKnown(*info)
	}

	certPool, err := util.InitCertificatePool(clientConfiguration.CertificateFilename)
	if err != nil {
		return fmt.Errorf("unable to initialize client: %s", err)
	}
	clientConfiguration.CertificatePool = certPool
	c.configuration = clientConfiguration

	// Call well known endpoint
	err = c.initializeWellKnown()
	if err != nil {
		return fmt.Errorf("unable to initialize client (well-known): %s", err)
	}

	err = c.checkLabelingRequiredAndSupported(clientConfiguration.Labels)
	if err != nil {
		return err
	}

	localRequestDto := logic.CreateRequestConfiguration(*clientConfiguration)

	c.configurationRequestDto = &localRequestDto
	c.initChangeDetector(changeDetector)

	// If client is disabled, just set RefreshMode to NEVER.
	// No further initialization is required.
	if c.configuration.Disable {
		never := config.RefreshModeNever
		c.configuration.RefreshMode = &never
		return nil
	}

	// Initialize configuration data
	if *c.configuration.InitializationMode == config.InitializationModeEager {
		err := c.initializeValues()
		if err != nil {
			return err
		}
	}
	if *c.configuration.InitializationMode == config.InitializationModeEagerAsync {
		go func() {
			err := c.initializeValues()
			if err != nil {
				log.Warn(err)
			}
		}()
	}
	if *c.configuration.InitializationMode == config.InitializationModeLazy {
		// don't initialize data
	}

	// Start timer if RefreshMode is TIMER
	if *c.configuration.RefreshMode == config.RefreshModeTimer {
		c.setupTimer()
	}

	return nil
}

func (c *ClientImplementation) initChangeDetector(changeDetector logic.ConfigurationChangeDetector) {
	if changeDetector == nil {
		// Set default change detector if no detector was explicitely set
		c.changeDetector = &logic.UniqueKeyConfigurationChangeDetector{}
	} else {
		c.changeDetector = changeDetector
	}
}

func (c *ClientImplementation) ResetInitialization() {
	c.configuration = nil
	c.configurationRequestDto = nil
}

func (c *ClientImplementation) GetConfig() *config.ClientConfiguration {
	return c.configuration
}

func (c *ClientImplementation) GetWellKnown() *model.WellKnown {
	return &c.wellknown
}

// setupTimer sets up the timer for periodically updating the configuration data
func (c *ClientImplementation) setupTimer() {
	log.Info("Started Timer")
	ticker := time.NewTicker(time.Millisecond * time.Duration(*c.configuration.RefreshCycle))
	go func() {
		for range ticker.C {
			if c.refreshNeeded() {
				err := c.refreshValues()
				if err != nil {
					log.Warnf("Unable to refresh configuration data with timer: %s", err)
				}
			}
		}
	}()
}

func (c *ClientImplementation) LogConfiguration() {
	configuration := c.GetConfig()

	if configuration == nil {
		log.Warnf("Go-client configuration is nil")
	} else {
		data, err := json.Marshal(configuration)

		if err != nil {
			log.Errorf("Unable to log Go-client configuration: %s", err)
		}

		log.Infof("Go-client configuration: %s ", string(data))
	}
}

// GetConfigValues returns the config values.
//
// Important notice: No copy of the data is made. Don't modify the returned data!
func (c *ClientImplementation) GetConfigValues() ([]model.ConfigValueDto, error) {
	err := c.initializeValues()
	if err != nil {
		return nil, err
	}

	if c.refreshNeeded() && *c.configuration.RefreshMode == config.RefreshModeLazy {
		err := c.refreshValues()
		if err != nil {
			return nil, err
		}
	}

	return c.cachedConfigValues, nil
}

// GetConfigValue returns the value for the given key.
//
// Precondition:
// The key should be unique, this function just returns the first ConfigValue with the given key.
func (c *ClientImplementation) GetConfigValue(key string) (model.ConfigValueDto, error) {
	configValues, err := c.GetConfigValues()

	if err != nil {
		return model.ConfigValueDto{}, err
	}

	for _, configValue := range configValues {
		if key == configValue.Key {
			return configValue, nil
		}
	}

	return model.ConfigValueDto{}, fmt.Errorf("ConfigValue with key %s doesn't exist", key)
}

func (c *ClientImplementation) initializeWellKnown() error {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	var tryCount = 1
	var maxRetries = 1
	if c.configuration != nil && c.configuration.MaxRetries != nil {
		maxRetries = *c.configuration.MaxRetries + 1
	}
	for tryCount <= maxRetries {

		wellKnown, err := c.WellknownBackend.GetWellKnownFromConfigSeeder(c.configuration)

		if err == nil {
			c.wellknown = *wellKnown
			return nil
		}

		var retryableError *backend.RetryableError
		if errors.As(err, &retryableError) {
			log.Warnf("Timeout occured while accessing ConfigSeeders well known endpoint. Retry %d of %d, sleep %dms. Reason: %s",
				tryCount, *c.configuration.MaxRetries, *c.configuration.RetryWaitTime, retryableError)

			time.Sleep(time.Millisecond * time.Duration(*c.configuration.RetryWaitTime))
		}

		tryCount++
	}

	return fmt.Errorf("unable to access ConfigSeeders well known endpoint after %d retries", *c.configuration.MaxRetries)
}

// initializeValues initializes the values - only loads values if not initialized yet
func (c *ClientImplementation) initializeValues() error {

	if !c.valuesInitialized {
		err := c.refreshValues()
		if err != nil {
			return err
		} else {
			c.valuesInitialized = true
		}
	}

	return nil
}

func (c *ClientImplementation) ForcedRefresh() error {
	return c.refreshValues()
}

func (c *ClientImplementation) refreshValues() error {
	if c.configuration.RefreshMode != nil && *c.configuration.RefreshMode == config.RefreshModeNever {
		log.Info("Refresh prevented because of RefreshMode 'NEVER'.")
		return nil
	}

	c.mutex.Lock()
	defer c.mutex.Unlock()

	var tryCount = 1
	for tryCount <= *c.configuration.MaxRetries+1 {
		request := c.configurationRequestDto

		// If no request time was specified in the configuration, set request time to now
		if c.configuration.DateTime == nil {
			now := time.Now()
			request.DateTime = &now
		}

		newConfigValues, err := c.configurationBackend.GetConfigValuesFromConfigSeeder(request, c.configuration)

		if err == nil {
			c.lastSynchronization = util.MakeTimestamp()
			oldConfigValues := c.cachedConfigValues
			c.cachedConfigValues = *newConfigValues

			err = c.notifyUpdates(oldConfigValues, c.cachedConfigValues)
			if err != nil {
				return fmt.Errorf("error occured while notifying updates. Reason: %s", err)
			}

			return nil
		}

		var retryableError *backend.RetryableError
		if errors.As(err, &retryableError) {
			log.Warnf("Timeout occured while reading configuration from ConfigSeeder. Retry %d of %d, sleep %dms. Reason: %s",
				tryCount, *c.configuration.MaxRetries, *c.configuration.RetryWaitTime, retryableError)

			time.Sleep(time.Millisecond * time.Duration(*c.configuration.RetryWaitTime))
		} else {
			return fmt.Errorf("error occured while reading configuration from ConfigSeeder. Reason: %s", err)
		}

		tryCount++
	}

	return fmt.Errorf("unable to access ConfigSeeder after %d retries, local data not updated", *c.configuration.MaxRetries)
}

func (c *ClientImplementation) notifyUpdates(oldData []model.ConfigValueDto, newData []model.ConfigValueDto) error {
	log.Debug("NotifyUpdates called")
	changedValues, err := c.changeDetector.GetChangedValues(newData, oldData, c.configuration)
	if err != nil {
		return err
	}

	// notify listeners
	if len(changedValues) > 0 {
		for _, listener := range c.keyUpdateListeners {
			listener(changedValues)
		}
	}

	return nil
}

// refreshNeeded checks if the values need to be refreshed (lastSynchronization - refreshCycle < now)
func (c *ClientImplementation) refreshNeeded() bool {
	if *c.configuration.RefreshMode == config.RefreshModeManual {
		return false
	}

	nextUpdate := c.lastSynchronization + int64(*c.configuration.RefreshCycle) - 100
	now := util.MakeTimestamp()
	return nextUpdate < now
}

func (c *ClientImplementation) Subscribe(listener KeyUpdateListener) {
	c.keyUpdateListeners = append(c.keyUpdateListeners, listener)
}

func (c *ClientImplementation) checkLabelingRequiredAndSupported(labels []string) error {
	if len(labels) == 0 {
		return nil
	}

	configSeederVersionString, versionAvailable := c.wellknown.GetVersion()
	log.Infof("Configseederversion: %s, ok %t", configSeederVersionString, versionAvailable)
	if !versionAvailable {
		return fmt.Errorf("labels are configured but ConfigSeeder version can't be retrieved. Needs to be at least version %s", CONFIGSEEDER_VERSION_WITH_LABELING_SUPPORT)
	}

	configSeederVersion, err := semver.NewVersion(configSeederVersionString)
	if err != nil {
		return fmt.Errorf("unable to convert version %s to semver object: %s", configSeederVersionString, err)
	}

	configSeederVersionWithLabelSupport, _ := semver.NewVersion(CONFIGSEEDER_VERSION_WITH_LABELING_SUPPORT)

	if configSeederVersion.LessThan(configSeederVersionWithLabelSupport) {
		return fmt.Errorf("labels are configured but ConfigSeeder version %s is older than the required minimum version %s",
			configSeederVersionString, CONFIGSEEDER_VERSION_WITH_LABELING_SUPPORT)
	}

	return nil
}
