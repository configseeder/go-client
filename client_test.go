package configseeder

import (
	"errors"
	"flag"
	log "github.com/sirupsen/logrus"
	"gitlab.com/configseeder/go-client/backend"
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
	"gitlab.com/configseeder/go-client/util_test"
	"testing"
)

func TestClientImplementation_GetConfigUninitialized(t *testing.T) {

	client := ClientImplementation{}
	actual := client.GetConfig()

	if actual != nil {
		t.Errorf("Config must be nil if init() wasn't called")
	}
}

func TestClientImplementation_InitWithoutInitFlags(t *testing.T) {
	client := ClientImplementation{}
	err := client.Init(nil)

	if err == nil {
		t.Errorf("Init() should return error if InitFlags wasn't called before")
	}
}

func TestClientImplementation_Init(t *testing.T) {
	// Initialize Client
	initProperties()

	client := ClientImplementation{
		WellknownBackend: &WellKnownBackendMock{&model.WellKnown{}},
	}
	err := client.Init(nil)

	if err != nil {
		t.Fatalf("Init() shouldn't return error but got %s", err)
	}

	if client.configuration == nil {
		t.Errorf("clientConfiguration mustn't be nil if Init() was called")
	}
	if client.configurationRequestDto == nil {
		t.Errorf("configurationRequestDto mustn't be nil if Init() was called")
	}
}

func TestClientImplementation_GetConfig(t *testing.T) {
	// Initialize Client
	initProperties()

	client := ClientImplementation{
		WellknownBackend: &WellKnownBackendMock{&model.WellKnown{}},
	}
	err := client.Init(nil)

	if err != nil {
		t.Fatalf("Init() shouldn't return error but got %s", err)
	}

	actual := client.GetConfig()

	if actual == nil {
		t.Errorf("GetConfig() mustn't return nil if Init() was called")
	}
}

func initProperties() {
	var properties = map[string]string{
		config.ConfigseederClientApiKey:                 "eyJraWQiOiJjb25maWctc2VydmVyLWp3dC1pZCIsImFsZyI6IlJTNTEyIn0.eyJ0eXBlIjoiYXBpa2V5IiwidHlwZSI6ImFwaS1rZXkiLCJpc3MiOiJDb25maWdTZWVkZXJNYW5hZ2VtZW50IiwiaWF0IjoxNTQ0Mzg5ODA3LCJleHAiOjE1NDg5NzkyMDAsInN1YiI6Ikt1YmVybmV0ZXNDb25uZWN0b3JUb2tlbiIsImFwaS10b2tlbi1pZCI6Ijk1N2JkZTIxLTI0ZjctNGQ2Ny1iM2NiLTJmMDJhYzUwYTA3MCIsInRlbmFudC1pZCI6ImYxZTkzNTk3LTM0MzMtNDAxNy1iNWY2LTQzYTlkNWMzYWFiMCIsImFjY2VzcyI6eyJhbGxFbnZpcm9ubWVudHMiOnRydWUsImFsbENvbmZpZ3VyYXRpb25Hcm91cHMiOmZhbHNlLCJjb25maWd1cmF0aW9uLWdyb3VwcyI6W3siaWQiOiJkYjFiZDkzYi1iZGNjLTRkZGYtODJkZC05YmI1N2U1YWMwMWEiLCJrZXkiOiJrdWJlcm5ldGVzLWNvbm5lY3RvciJ9XX0sImF1dGhvcml0aWVzIjpbIkFQSV9LRVlfQ1JFQVRFIl19.dE6tsthBVbJWrzIEMkWgKMTZ-kIOSeK8Y7wzDZriJIzKyfFVXuGp2l9rQUKQWSEeGZe5Sgm42KZxT_GsSJEok85O0GdmG8UuVNkR9RrP540-RDnXP7htQlyhyBpOlk6lZc75D6iEGeE3q7f4w2dOsBHKeZ7jPbcE2vYZlaEQ3mE",
		config.ConfigseederClientConfigurationGroupKeys: "group1, group2",
		config.ConfigseederClientEnvironmentKey:         "environmentKey",
		config.ConfigseederClientInitializationMode:     string(config.InitializationModeLazy),
		config.ConfigseederClientRefreshMode:            string(config.RefreshModeLazy),
		config.ConfigseederClientServerUrl:              "http://serverUrl",
	}
	util_test.ResetFlagsAndEnvironment(properties)

	client := ClientImplementation{}
	client.ResetInitialization()
	client.InitFlags()
	for k, v := range properties {
		flag.Set(k, v)
	}
	flag.Parse()
}

func TestClientImplementation_notifyUpdates(t *testing.T) {
	var updatedData []model.ConfigValueChange

	testListener := func(data []model.ConfigValueChange) {
		updatedData = data
	}

	var configValuesOld []model.ConfigValueDto = nil

	key := "key"
	value := "value"
	var lastUpdateInMilliseconds int64 = 1

	var configValuesNew = []model.ConfigValueDto{
		{
			Key:                      key,
			LastUpdateInMilliseconds: lastUpdateInMilliseconds,
			Value:                    value,
		},
	}

	client := ClientImplementation{}
	client.initChangeDetector(nil)
	client.Subscribe(testListener)
	client.notifyUpdates(configValuesOld, configValuesNew)

	expectedUpdateCount := 1
	if len(updatedData) != expectedUpdateCount {
		t.Fatalf("Expected to get %d updates but got %d", expectedUpdateCount, len(updatedData))
	}
}

func TestClientImplementation_refreshNeededManual(t *testing.T) {
	client := initializeClientWithoutBackend(config.RefreshModeManual, 0)
	actual := client.refreshNeeded()
	if actual {
		t.Error("No refresh should be needed in refreshMode Manual")
	}
}

func TestClientImplementation_refreshNeededNo(t *testing.T) {
	client := initializeClientWithoutBackend(config.RefreshModeLazy, 100)
	actual := client.refreshNeeded()
	if actual {
		t.Error("No refresh should be needed because refreshCycle not done yet")
	}
}
func TestClientImplementation_refreshNeededYes(t *testing.T) {
	client := initializeClientWithoutBackend(config.RefreshModeLazy, 0)
	actual := client.refreshNeeded()
	if !actual {
		t.Error("Refresh should be needed because refreshCycle is done")
	}
}

func TestClientImplementation_refreshValuesNever(t *testing.T) {
	client, _ := initializeClientWithMockedBackend(config.RefreshModeNever, 0, 0, 0, "")

	err := client.refreshValues()
	if err != nil {
		t.Errorf("refreshValues() shouldn't return error when called with refresh mode never: %s", err)
	}

	if client.cachedConfigValues != nil {
		t.Fatalf("cachedConfigValues should be nil after refreshValues() was called")
	}
}

func TestClientImplementation_refreshValues(t *testing.T) {
	client, _ := initializeClientWithMockedBackend(config.RefreshModeLazy, 100, 1, 0, "")

	err := client.refreshValues()
	if err != nil {
		t.Fatalf("refreshValues() shouldn't return error: %s", err)
	}

	if client.cachedConfigValues == nil {
		t.Fatalf("cachedConfigValues shouldn't be nil after refreshValues() was called")
	}

	expectedValueCount := 1
	if len(client.cachedConfigValues) != expectedValueCount {
		t.Errorf("Expeced cachedConfigValues to contain %d elements but was %d",
			expectedValueCount, len(client.cachedConfigValues))
	}
}

// Testcase where a retryable error is returned / configuration values are given back / no error is given back
func TestClientImplementation_refreshValuesWithOkError(t *testing.T) {
	client, mockedBackend := initializeClientWithMockedBackend(config.RefreshModeLazy, 100, 1, 10, "")

	mockedBackend.errorToReturn = &backend.RetryableError{}
	mockedBackend.errorCount = 1

	err := client.refreshValues()
	if err != nil {
		t.Fatalf("refreshValues() shouldn't return error: %v", err)
	}

	if client.cachedConfigValues == nil {
		t.Fatalf("cachedConfigValues shouldn't be nil after refreshValues() was called")
	}

	expectedValueCount := 1
	if len(client.cachedConfigValues) != expectedValueCount {
		t.Errorf("Expeced cachedConfigValues to contain %d elements but was %d",
			expectedValueCount, len(client.cachedConfigValues))
	}
}

func TestClientImplementation_refreshValuesWithTooManyErrors(t *testing.T) {
	client, mockedBackend := initializeClientWithMockedBackend(config.RefreshModeLazy, 100, 1, 10, "")

	mockedBackend.errorToReturn = &backend.RetryableError{}
	mockedBackend.errorCount = 2

	err := client.refreshValues()
	if err == nil {
		t.Fatalf("refreshValues() should return error but didn't")
	}

	if client.cachedConfigValues != nil {
		t.Fatalf("cachedConfigValues should be nil after refreshValues() was called")
	}
}
func TestClientImplementation_refreshValuesWithNonRetrieableError(t *testing.T) {
	client, mockedBackend := initializeClientWithMockedBackend(config.RefreshModeLazy, 100, 1, 10, "")

	mockedBackend.errorCount = 1
	mockedBackend.errorToReturn = errors.New("abc")

	err := client.refreshValues()
	if err == nil {
		t.Fatalf("refreshValues() should return an error but didn't")
	}

	if client.cachedConfigValues != nil {
		t.Fatalf("cachedConfigValues should be nil after refreshValues() was called")
	}
}

func TestCheckLabelingRequiredAndSupported_SupportedNotRequired(t *testing.T) {
	// given
	client, _ := initializeClientWithMockedBackend(config.RefreshModeLazy, 100, 1, 10, "2.25.0")

	// when
	err := client.checkLabelingRequiredAndSupported([]string{})

	// then
	if err != nil {
		t.Errorf("Expected check to return no error but got %s", err)
	}
}

func TestCheckLabelingRequiredAndSupported_SupportedAndRequired(t *testing.T) {
	// given
	client, _ := initializeClientWithMockedBackend(config.RefreshModeLazy, 100, 1, 10, "2.25.0")

	// when
	err := client.checkLabelingRequiredAndSupported([]string{"configured-label"})

	// then
	if err != nil {
		t.Errorf("Expected check to return no error but got %s", err)
	}
}

func TestCheckLabelingRequiredAndSupported_NotSupportedNotRequired(t *testing.T) {
	// given
	client, _ := initializeClientWithMockedBackend(config.RefreshModeLazy, 100, 1, 10, "2.24.0")

	// when
	err := client.checkLabelingRequiredAndSupported([]string{})

	// then
	if err != nil {
		t.Errorf("Expected check to return no error but got %s", err)
	}
}

func TestCheckLabelingRequiredAndSupported_NotSupportedAndRequired(t *testing.T) {
	// given
	client, _ := initializeClientWithMockedBackend(config.RefreshModeLazy, 100, 1, 10, "2.24.0")

	// when
	err := client.checkLabelingRequiredAndSupported([]string{"configured-label"})

	// then
	if err == nil {
		t.Errorf("Expected check to return error but didn't")
	}
}

func initializeClientWithoutBackend(refreshMode config.RefreshMode, refreshCycle int) *ClientImplementation {
	var wellKnown = model.WellKnown{}

	client := ClientImplementation{
		WellknownBackend: &WellKnownBackendMock{wellKnown: &wellKnown},
		configuration: &config.ClientConfiguration{
			RefreshMode:  &refreshMode,
			RefreshCycle: &refreshCycle,
		},
		lastSynchronization: util.MakeTimestamp(),
	}

	return &client
}

func initializeClientWithMockedBackend(refreshMode config.RefreshMode, refreshCycle int, maxRetries int, retryWaitTime int,
	configseederVersion string) (*ClientImplementation, *BackendMock) {

	var mockedBackend = &BackendMock{
		values: []model.ConfigValueDto{
			{},
		},
	}

	var wellKnown = model.WellKnown{
		Release: map[string]string{
			"version": configseederVersion,
		},
	}

	client := &ClientImplementation{
		configurationBackend: mockedBackend,
		WellknownBackend: &WellKnownBackendMock{
			wellKnown: &wellKnown,
		},
		wellknown: wellKnown,
		configuration: &config.ClientConfiguration{
			RefreshMode:   &refreshMode,
			RefreshCycle:  &refreshCycle,
			MaxRetries:    &maxRetries,
			RetryWaitTime: &retryWaitTime,
		},
		configurationRequestDto: &model.ConfigurationRequestDto{},
	}
	client.initChangeDetector(nil)
	return client, mockedBackend
}

type WellKnownBackendMock struct {
	wellKnown *model.WellKnown
}

func (b *WellKnownBackendMock) GetWellKnownFromConfigSeeder(_ *config.ClientConfiguration) (*model.WellKnown, error) {
	return b.wellKnown, nil
}

type BackendMock struct {
	values []model.ConfigValueDto

	errorCount    int
	errorToReturn error
}

func (b *BackendMock) GetConfigValuesFromConfigSeeder(_ *model.ConfigurationRequestDto, _ *config.ClientConfiguration) (*[]model.ConfigValueDto, error) {
	log.Infof("GetConfigurationValuesFromConfigSeeder called. Error count =%d, errorToReturn set %t", b.errorCount, b.errorToReturn != nil)
	if b.errorCount > 0 && b.errorToReturn != nil {
		b.errorCount--

		return nil, b.errorToReturn
	}

	return &b.values, nil
}
