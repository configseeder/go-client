package configseeder

import (
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/logic"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
)

type Client interface {
	// InitFlags Initializes the flags which can be used for configuring the ConfigSeeder access.
	// It is required, that this method is called before flags.Parse().
	// InitFlags() and flags.Parse() must be called before accessing the ConfigSeeder / calling GetConfigurationGroups.
	//
	InitFlags()

	// Init initializes the ConfigSeeder go Client.
	// Requires, that InitFlags() / flags.Parse() was called before.
	//
	// changeDetector determines how changed configValues are detected (if nil the standard detector is used)
	//
	Init(changeDetector logic.ConfigurationChangeDetector) error

	// InitWith initializes the ConfigSeeder go Client with given configuration.
	//
	// userAgentInfo  containes information about the environment accessing ConfigSeeder
	// changeDetector determines how changed configValues are detected (if nil the standard detector is used)
	//
	InitWith(configuration *config.ClientConfiguration, userAgentInfo *util.UserAgentInfo,
		changeDetector logic.ConfigurationChangeDetector) error

	// ResetInitialization resets the cached configuration and clientRequest.
	// This method shouldn't be called during normal operation.
	//
	ResetInitialization()

	// GetConfig returns the config of the client. No Initialization will be done if config is nil.
	//
	GetConfig() *config.ClientConfiguration

	// GetWellKnown returns the well-known information retrieved by ConfigSeeder.
	GetWellKnown() *model.WellKnown

	// LogConfiguration logs the configuration
	//
	LogConfiguration()

	// GetConfigValues returns the config values.
	//
	// Important notice: No copy of the data is made. Don't modify the returned data!
	//
	GetConfigValues() ([]model.ConfigValueDto, error)

	// GetConfigValue returns the value for the given key.
	//
	// Precondition:
	// The key should be unique, this function just returns the first ConfigValue with the given key.
	//
	GetConfigValue(key string) (model.ConfigValueDto, error)

	// ForcedRefresh force reloads the data.
	//
	// Normally used with RefreshMode Manual
	// If RefreshMode is set to Never, no refresh is done.
	//
	ForcedRefresh() error

	// Subscribe adds the listener to the list of known listeners.
	//
	// Listeners will be updated if a configValue was added, removed or modified.
	//
	Subscribe(listener KeyUpdateListener)
}

// KeyUpdateListener is a listener that can be notified whe configuration values change.
//
// Parameter keys which have been update (true = key exists, false = key has been removed)
//
type KeyUpdateListener func([]model.ConfigValueChange)
