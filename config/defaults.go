package config

func GetRefreshCycle() *int {
	value := RefreshCycle
	return &value
}

func GetDefaultTimeout() *int {
	value := DefaultTimeout
	return &value
}

func GetReadTimeout() *int {
	value := ReadTimeout
	return &value
}

func GetMaxRetries() *int {
	value := MaxRetries
	return &value
}

func GetRetryWaitTime() *int {
	value := RetryWaitTime
	return &value
}
