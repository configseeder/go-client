/*
 * Copyright (c) 2024 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package config

import (
	"errors"
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
	"gopkg.in/yaml.v2"
	"net/url"
	"os"
	"strings"
)

// GetConfiguration loads the configseeder configuration from the configfile / parameters an environment variables.
// The configuration is validated, an error is returned if the configuration is not valid.
func GetConfiguration() (*ClientConfiguration, error) {

	clientConfiguration := loadConfigurationFromYaml()

	overrideWithParameter(clientConfiguration)

	return GetValidConfiguration(clientConfiguration)
}

// GetValidConfiguration can be used to validate a manually initialized configuration.
// - Configurations is initialized with the values set in ConfigurationGroupKeys if required
// - Defaults are set if required
// - Configuration is validated
//
// If the configuration isn't valid, an error is returned
func GetValidConfiguration(clientConfiguration *ClientConfiguration) (*ClientConfiguration, error) {
	MergeConfigurationGroupKeysToConfigurations(clientConfiguration)
	setDefaultsIfRequired(clientConfiguration)

	err := validate(clientConfiguration)
	if err != nil {
		return nil, err
	}

	return clientConfiguration, nil
}

// loadConfigurationFromYaml loads configuration from configseeder.yaml
// If the file doesn't exist, an empty configuration is returned
func loadConfigurationFromYaml() *ClientConfiguration {
	configfileFlag := flag.Lookup(ConfigseederConfigfile)
	if configfileFlag == nil {
		log.Info("Unable to determine name of configfile (InitFlags() called?)")
		return &ClientConfiguration{}
	}

	configfile := configfileFlag.Value.String()
	if _, err := os.Stat(configfile); os.IsNotExist(err) {
		log.Infof("ConfigSeeder configuration not found (%s)", configfile)
		return &ClientConfiguration{}
	} else {
		// Read existing configuration

		yamlFile, err := os.ReadFile(configfile)
		if err != nil {
			log.Errorf("Unable to read configuration from file '%s': #%v ", configfile, err)
		} else {
			root := configurationRoot{}

			err = yaml.Unmarshal(yamlFile, &root)
			if err != nil {
				log.Errorf("Unable to unmarshal configuration from file '%s': #%v ", configfile, err)
			} else if root.Configuration == nil || root.Configuration.ClientConfiguration == nil {
				log.Warnf("Configuration file '%s' doesn't contain configseeder or configseeder->client", configfile)
			} else {
				return root.Configuration.ClientConfiguration
			}
		}

		return &ClientConfiguration{}
	}
}

func InitFlags() {
	flag.String(ConfigseederConfigfile, "configseeder.yaml", "Configfile for ConfigSeeder Access")

	flag.String(ConfigseederClientApiKey, "", "API Key")
	flag.String(ConfigseederClientCertificateFilename, "", "Name of a file containing additional (pem formatted) certificates for tls verification")
	flag.String(ConfigseederClientConfigurationGroupKeys, "", "List of the ConfigurationGroups, which should be loaded with Mode LATEST")
	flag.String(ConfigseederClientConnectionTimeout, "", fmt.Sprintf("How long the client shall wait for initial connection (default: %d)", DefaultTimeout))
	flag.String(ConfigseederClientContext, "", "Filter: Context")
	flag.String(ConfigseederClientDateTime, "", "Filter: Date Time")
	flag.String(ConfigseederClientDisable, "", "Disable configseeder client with true")
	flag.String(ConfigseederClientEnvironmentKey, "", "Key of the environment")
	flag.String(ConfigseederClientFullForNonFilteredKey, "", "Set to true to disable duplicate elimination (default: false)")
	flag.String(ConfigseederClientInitializationMode, "", "How all values should be initialized. Options: EAGER, EAGER_ASYNC, LAZY")
	flag.String(ConfigseederClientMaxRetries, "", fmt.Sprintf("How many times connection retry should be performed (default: %d)", MaxRetries))
	flag.String(ConfigseederClientReadTimeout, "", fmt.Sprintf("How long an answer from the server shall take max (default: %d)", ReadTimeout))
	flag.String(ConfigseederClientRefreshMode, "", "When the values shall be refreshed. Options: MANUAL, LAZY, TIMER")
	flag.String(ConfigseederClientRefreshCycle, "", fmt.Sprintf("How often values get refreshed (in milliseconds, default: %d)", RefreshCycle))
	flag.String(ConfigseederClientRetryWaitTime, "", fmt.Sprintf("How many milliseconds should be waited before a new request is performed (default: %d)", RetryWaitTime))
	flag.String(ConfigseederClientServerUrl, "", "URL of the config server")
	flag.String(ConfigseederClientTenantKey, "", "Key of the tenant")
	flag.String(ConfigseederClientVersion, "", "Filter: Version")
}

/*
 * Overrides given configuration with values set in environment or startparameter.
 * A startparameter has higher priority than an environment variable.
 */
func overrideWithParameter(clientConfiguration *ClientConfiguration) {
	if value, exists := util.GetParameter(ConfigseederClientApiKey); exists {
		clientConfiguration.ApiKey = &value
	}
	if value, exists := util.GetParameter(ConfigseederClientCertificateFilename); exists {
		clientConfiguration.CertificateFilename = &value
	}
	if value, exists := util.GetParameter(ConfigseederClientConfigurationGroupKeys); exists {
		clientConfiguration.ConfigurationGroupKeys = &value
	}
	if value, exists := util.GetIntParameter(ConfigseederClientConnectionTimeout); exists {
		clientConfiguration.ConnectionTimeout = &value
	}
	if value, exists := util.GetParameter(ConfigseederClientContext); exists {
		clientConfiguration.Context = &value
	}
	if value, exists := util.GetDateTimeParameter(ConfigseederClientDateTime); exists {
		clientConfiguration.DateTime = value
	}
	if value, exists := util.GetBoolParameter(ConfigseederClientDisable); exists {
		clientConfiguration.Disable = value
	}
	if value, exists := util.GetParameter(ConfigseederClientEnvironmentKey); exists {
		clientConfiguration.EnvironmentKey = &value
	}
	if value, exists := util.GetBoolParameter(ConfigseederClientFullForNonFilteredKey); exists {
		clientConfiguration.FullForNonFiltered = &value
	}
	if value, exists := util.GetParameter(ConfigseederClientInitializationMode); exists {
		clientConfiguration.InitializationMode = GetInitializationMode(value)
	}
	if value, exists := util.GetIntParameter(ConfigseederClientMaxRetries); exists {
		clientConfiguration.MaxRetries = &value
	}
	if value, exists := util.GetIntParameter(ConfigseederClientReadTimeout); exists {
		clientConfiguration.ReadTimeout = &value
	}
	if value, exists := util.GetParameter(ConfigseederClientRefreshMode); exists {
		clientConfiguration.RefreshMode = GetRefreshMode(value)
	}
	if value, exists := util.GetIntParameter(ConfigseederClientRefreshCycle); exists {
		clientConfiguration.RefreshCycle = &value
	}
	if value, exists := util.GetIntParameter(ConfigseederClientRetryWaitTime); exists {
		clientConfiguration.RetryWaitTime = &value
	}
	if value, exists := util.GetParameter(ConfigseederClientServerUrl); exists {
		clientConfiguration.ServerUrl = &value
	}
	if value, exists := util.GetParameter(ConfigseederClientTenantKey); exists {
		clientConfiguration.TenantKey = value
	}
	if value, exists := util.GetParameter(ConfigseederClientVersion); exists {
		clientConfiguration.Version = &value
	}
}

// MergeConfigurationGroupKeysToConfigurations merges configurationGroupKeys into Configurations.
// No Version is used, SelectionMode is LATEST.
func MergeConfigurationGroupKeysToConfigurations(clientConfiguration *ClientConfiguration) {
	if clientConfiguration.ConfigurationGroupKeys == nil {
		return
	}

	groups := strings.Split(*clientConfiguration.ConfigurationGroupKeys, ",")
	for _, group := range groups {
		var version *string = nil
		selectionMode := model.SelectionModeLatest

		groupConfiguration := GroupConfiguration{
			GroupKey:      strings.TrimSpace(group),
			Version:       version,
			SelectionMode: &selectionMode,
		}

		clientConfiguration.Configurations = append(clientConfiguration.Configurations, groupConfiguration)
	}
}

/*
 * Set empty values to default, if a configuration is requred (timeouts).
 * Don't set a default, if no value is required.
 */
func setDefaultsIfRequired(clientConfiguration *ClientConfiguration) {
	// Refresh
	if clientConfiguration.RefreshCycle == nil &&
		clientConfiguration.RefreshMode != nil && *clientConfiguration.RefreshMode != RefreshModeManual {
		clientConfiguration.RefreshCycle = GetRefreshCycle()
	}
	// Retries
	if clientConfiguration.MaxRetries == nil {
		clientConfiguration.MaxRetries = GetMaxRetries()
	}
	if clientConfiguration.RetryWaitTime == nil {
		clientConfiguration.RetryWaitTime = GetRetryWaitTime()
	}
	// Timeouts
	if clientConfiguration.ConnectionTimeout == nil {
		clientConfiguration.ConnectionTimeout = GetDefaultTimeout()
	}
	if clientConfiguration.ReadTimeout == nil {
		clientConfiguration.ReadTimeout = GetReadTimeout()
	}
}

/*
 * Validates the given configuration and returns an error if required
 */
func validate(configuration *ClientConfiguration) error {

	errorCollection := model.NewErrorCollection("go-client configuration has validation errors:")

	validateConnectionParameters(configuration, errorCollection)
	validateRefreshMode(configuration, errorCollection)
	validateInitialisationMode(configuration, errorCollection)
	validateConfigurations(configuration.Configurations, errorCollection)
	validateEnvironment(configuration, errorCollection)
	validateContextAndLabels(configuration, errorCollection)

	if errorCollection.HasError() {
		return errorCollection
	}

	return nil
}

/*
 * Validates connection parameters like url, API Key and Timeouts.
 */
func validateConnectionParameters(configuration *ClientConfiguration, errorCollection *model.ErrorCollection) {
	// Server URL
	if configuration.ServerUrl == nil {
		errorCollection.Add(errors.New("serverUrl must not be null"))
	} else {
		_, err := url.ParseRequestURI(*configuration.ServerUrl)
		if err != nil {
			errorCollection.Add(fmt.Errorf("serverUrl '%s' is not a valid url %s", *configuration.ServerUrl, err))
		}
	}

	// API Key
	if configuration.ApiKey == nil {
		errorCollection.Add(errors.New("apiKey must not be null"))
	}

	// Timeouts
	if configuration.ConnectionTimeout == nil {
		errorCollection.Add(errors.New("connectionTimeout must not be null"))
	} else if *configuration.ConnectionTimeout < 0 {
		errorCollection.Add(errors.New("connectionTimeout must not be negative"))
	}
	if configuration.ReadTimeout == nil {
		errorCollection.Add(errors.New("readTimeout must not be null"))
	} else if *configuration.ReadTimeout < 0 {
		errorCollection.Add(errors.New("readTimeout must not be negative"))
	}

	// Retry
	if configuration.MaxRetries == nil {
		errorCollection.Add(errors.New("maxRetries must not be null"))
	} else if *configuration.MaxRetries < MaxRetryLowerLimit || *configuration.MaxRetries > MaxRetryHigherLimit {
		errorCollection.Add(fmt.Errorf("maxRetries must be between %d and %d but is %d",
			MaxRetryLowerLimit, MaxRetryHigherLimit, *configuration.MaxRetries))
	}
	if configuration.RetryWaitTime == nil {
		errorCollection.Add(errors.New("retryWaitTime must not be null"))
	} else if *configuration.RetryWaitTime < 0 {
		errorCollection.Add(errors.New("retryWaitTime must not be negative"))
	}
}

/*
 * Validates RefreshMode and RefreshCycle
 */
func validateRefreshMode(configuration *ClientConfiguration, errorCollection *model.ErrorCollection) {
	if configuration.RefreshMode == nil {
		errorCollection.Add(errors.New("refreshMode must not be null"))
	} else if !IsValidRefreshMode(*configuration.RefreshMode) {
		errorCollection.Add(fmt.Errorf("refreshMode '%s' is not valid", *configuration.RefreshMode))
	} else if *configuration.RefreshMode != RefreshModeManual && configuration.RefreshCycle == nil {
		errorCollection.Add(fmt.Errorf("refreshMode must not be null when refreshMode is set to '%s'", *configuration.RefreshMode))
	}
}

/*
 * Validates InitialisationMode
 */
func validateInitialisationMode(configuration *ClientConfiguration, errorCollection *model.ErrorCollection) {
	if configuration.InitializationMode == nil {
		errorCollection.Add(errors.New("initializationMode must not be null"))
	} else if !IsValidInitializationMode(*configuration.InitializationMode) {
		errorCollection.Add(fmt.Errorf("initializationMode '%s' is not valid", *configuration.InitializationMode))
	}
}

/*
 * Validate Configurations (at least one entry, no duplicates)
 */
func validateConfigurations(configurations []GroupConfiguration, errorCollection *model.ErrorCollection) {
	if configurations == nil {
		errorCollection.Add(errors.New("configurations must not be null"))
	} else if len(configurations) == 0 {
		errorCollection.Add(errors.New("configurations must contain at least one configurationGroup"))
	}

	for _, configuration := range configurations {
		if configuration.Version != nil &&
			configuration.SelectionMode != nil && *configuration.SelectionMode != model.SelectionModeVersionNumber {

			errorCollection.Add(fmt.Errorf("if version number is set, selectionMode must be <nil> or VERSION_NUMBER but was %s",
				string(*configuration.SelectionMode)))
		}
	}

	encountered := map[string]bool{}
	for _, configuration := range configurations {
		if _, ok := encountered[configuration.GroupKey]; ok {
			errorCollection.Add(fmt.Errorf("configurationGroup '%s' is configured more than one time",
				configuration.GroupKey))
		}

		encountered[configuration.GroupKey] = true
	}
}

// validateEnvironment validates EnvironmentKey (must be set or nil)
func validateEnvironment(configuration *ClientConfiguration, errorCollection *model.ErrorCollection) {
	if configuration.EnvironmentKey != nil && len(*configuration.EnvironmentKey) == 0 {
		errorCollection.Add(errors.New("environment should either be null or have a value (length > 0)"))
	}
	if configuration.EnvironmentKey == nil &&
		(configuration.FullForNonFiltered == nil || !*configuration.FullForNonFiltered) {
		errorCollection.Add(errors.New("environment should not be null if not all entries should be returned"))
	}
}

// validateContextAndLabels ensures that the combination of context and labels is valid
// Supported is
// * context and labels is not set
// * either context or labels is set
func validateContextAndLabels(configuration *ClientConfiguration, errorCollection *model.ErrorCollection) {
	hasContext := configuration.Context != nil && len(*configuration.Context) > 0
	hasLabels := configuration.Labels != nil && len(configuration.Labels) > 0
	if hasContext && hasLabels {
		errorCollection.Add(errors.New("either set context or labels, not both"))
	}
}
