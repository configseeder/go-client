/*
 * Copyright (c) 2024 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package config

import (
	"crypto/x509"
	log "github.com/sirupsen/logrus"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
	"time"
)

type configurationRoot struct {
	Configuration *ConfigSeederConfiguration `yaml:"configseeder"`
}

type ConfigSeederConfiguration struct {
	ClientConfiguration *ClientConfiguration `yaml:"client"`
}

type ClientConfiguration struct {
	Disable                bool     `yaml:"disable"`
	TenantKey              string   `yaml:"tenantKey"`
	EnvironmentKey         *string  `yaml:"environmentKey"`
	FullForNonFiltered     *bool    `yaml:"fullForNonFiltered"`
	Version                *string  `yaml:"version"`
	Context                *string  `yaml:"context"`
	Labels                 []string `yaml:"labels"`
	DateTime               *time.Time
	Configurations         []GroupConfiguration `yaml:"configurations"`
	ConfigurationGroupKeys *string              `yaml:"configurationGroupKeys"`
	InitializationMode     *InitializationMode  `yaml:"initializationMode"`
	RefreshMode            *RefreshMode         `yaml:"refreshMode"`
	RefreshCycle           *int                 `yaml:"refreshCycle"`
	RetryWaitTime          *int                 `yaml:"retryWaitTime"`
	MaxRetries             *int                 `yaml:"maxRetries"`
	ConnectionTimeout      *int                 `yaml:"connectionTimeout"`
	ReadTimeout            *int                 `yaml:"readTimeout"`
	ServerUrl              *string              `yaml:"serverUrl"`
	ApiKey                 *string              `yaml:"apiKey"`

	CertificateFilename *string `yaml:"certificateFilename"`
	CertificatePool     *x509.CertPool
}

func (configuration *ClientConfiguration) UnmarshalYAML(unmarshal func(interface{}) error) error {
	type Alias ClientConfiguration
	aux := &struct {
		Disable                bool                 `yaml:"disable"`
		TenantKey              string               `yaml:"tenantKey"`
		EnvironmentKey         *string              `yaml:"environmentKey"`
		FullForNonFiltered     *bool                `yaml:"fullForNonFiltered"`
		Version                *string              `yaml:"version"`
		Context                *string              `yaml:"context"`
		Labels                 []string             `yaml:"labels"`
		DateTime               *string              `yaml:"dateTime"`
		ConfigurationGroupKeys *string              `yaml:"configurationGroupKeys"`
		Configurations         []GroupConfiguration `yaml:"configurations"`
		InitializationMode     *InitializationMode  `yaml:"initializationMode"`
		RefreshMode            *RefreshMode         `yaml:"refreshMode"`
		RefreshCycle           *int                 `yaml:"refreshCycle"`
		RetryWaitTime          *int                 `yaml:"retryWaitTime"`
		MaxRetries             *int                 `yaml:"maxRetries"`
		ConnectionTimeout      *int                 `yaml:"connectionTimeout"`
		ReadTimeout            *int                 `yaml:"readTimeout"`
		ServerUrl              *string              `yaml:"serverUrl"`
		ApiKey                 *string              `yaml:"apiKey"`

		CertificateFilename *string `yaml:"certificateFilename"`
	}{}

	if err := unmarshal(&aux); err != nil {
		return err
	}

	dateTime, err := util.FormattetTimeWithZoneToTime(aux.DateTime)
	if err != nil {
		return err
	}

	configuration.ApiKey = aux.ApiKey
	configuration.ConfigurationGroupKeys = aux.ConfigurationGroupKeys
	configuration.Configurations = aux.Configurations
	configuration.Context = aux.Context
	configuration.Labels = aux.Labels
	configuration.ConnectionTimeout = aux.ConnectionTimeout
	configuration.DateTime = dateTime
	configuration.Disable = aux.Disable
	configuration.EnvironmentKey = aux.EnvironmentKey
	configuration.FullForNonFiltered = aux.FullForNonFiltered
	configuration.InitializationMode = aux.InitializationMode
	configuration.MaxRetries = aux.MaxRetries
	configuration.ReadTimeout = aux.ReadTimeout
	configuration.RefreshCycle = aux.RefreshCycle
	configuration.RefreshMode = aux.RefreshMode
	configuration.RetryWaitTime = aux.RetryWaitTime
	configuration.ServerUrl = aux.ServerUrl
	configuration.TenantKey = aux.TenantKey
	configuration.Version = aux.Version
	configuration.CertificateFilename = aux.CertificateFilename

	return nil
}

type GroupConfiguration struct {
	ConfigurationGroupId string                                          `yaml:"configurationGroupId,omitempty"`
	GroupKey             string                                          `yaml:"groupKey,omitempty"`
	SelectionMode        *model.VersionedConfigurationGroupSelectionMode `yaml:"selectionMode,omitempty"`
	Version              *string                                         `yaml:"versionNumber,omitempty"`
}

type InitializationMode string

const (
	// InitializationModeEager will load values immediately after initialization
	InitializationModeEager InitializationMode = "EAGER"

	// InitializationModeEagerAsync will load values immediately (but asynchronously) after initialization
	InitializationModeEagerAsync InitializationMode = "EAGER_ASYNC"

	// InitializationModeLazy will load values when they are requested
	InitializationModeLazy InitializationMode = "LAZY"
)

var validInitializationModes = map[InitializationMode]bool{
	InitializationModeEager:      true,
	InitializationModeEagerAsync: true,
	InitializationModeLazy:       true,
}

func IsValidInitializationMode(mode InitializationMode) bool {
	_, ok := validInitializationModes[mode]

	return ok
}

func GetInitializationMode(value string) *InitializationMode {
	var initializationMode InitializationMode

	switch value {
	case string(InitializationModeEager):
		initializationMode = InitializationModeEager
	case string(InitializationModeEagerAsync):
		initializationMode = InitializationModeEagerAsync
	case string(InitializationModeLazy):
		initializationMode = InitializationModeLazy
	default:
		log.Warnf("Invalid InitializationMode '%s', resetting to <nil>", value)
		return nil
	}

	return &initializationMode
}

type RefreshMode string

const (
	// RefreshModeManual will refresh configuration data only when manually triggered
	RefreshModeManual RefreshMode = "MANUAL"

	// RefreshModeLazy will refresh configuration data only on request (blocking)
	RefreshModeLazy RefreshMode = "LAZY"

	// RefreshModeTimer will refresh values automatically with a background worker.
	RefreshModeTimer RefreshMode = "TIMER"

	// RefreshModeNever will never do a refresh. This is the case if the disable-Flag is set to true.
	RefreshModeNever RefreshMode = "NEVER"
)

var validRefreshModes = map[RefreshMode]bool{
	RefreshModeLazy:   true,
	RefreshModeManual: true,
	RefreshModeTimer:  true,
	RefreshModeNever:  true,
}

func IsValidRefreshMode(mode RefreshMode) bool {
	_, ok := validRefreshModes[mode]

	return ok
}

func GetRefreshMode(value string) *RefreshMode {
	var refreshMode RefreshMode

	switch value {
	case string(RefreshModeLazy):
		refreshMode = RefreshModeLazy
	case string(RefreshModeManual):
		refreshMode = RefreshModeManual
	case string(RefreshModeTimer):
		refreshMode = RefreshModeTimer
	case string(RefreshModeNever):
		refreshMode = RefreshModeNever
	default:
		log.Warnf("Invalid RefreshMode '%s', resetting to <nil>", value)
		return nil
	}

	return &refreshMode
}
