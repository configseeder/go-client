package config

import (
	"gitlab.com/configseeder/go-client/model"
	"gopkg.in/yaml.v2"
	"strconv"
	"testing"
	"time"
)

func TestClientConfiguration_UnmarshalYAMLNoConfigSeeder(t *testing.T) {
	data := ""

	root := configurationRoot{}

	yaml.Unmarshal([]byte(data), &root)

	if root.Configuration != nil {
		t.Fatal("Configuration should be <nil>")
	}
}

func TestClientConfiguration_UnmarshalYAMLNoConfigurationData(t *testing.T) {
	data := `
configseeder:
  client:
`

	root := configurationRoot{}

	yaml.Unmarshal([]byte(data), &root)

	configuration := root.Configuration

	if configuration == nil {
		t.Fatal("Configuration shouldn't be <nil>")
	}
	if configuration.ClientConfiguration != nil {
		t.Error("ClientConfiguration should be <nil>")
	}
}

func TestClientConfiguration_UnmarshalYAML(t *testing.T) {
	tenantKey := "tenantKey"
	environmentKey := "environmentKey"
	version := "version"
	context := "context"
	dateTime := "2001-02-03T04:05:06Z"
	initializationMode := InitializationModeEagerAsync
	refreshMode := RefreshModeLazy
	refreshCycle := 1
	retryWaitTime := 2
	maxRetries := 3
	readTimeout := 4
	connectionTimeout := 5
	serverUrl := "serverUrl"
	apiKey := "apiKey"

	configurationVersion := "configurationVersion"
	configurationGroup := "configurationGroup"
	configurationGroupId := "configurationGroupId"
	selectionMode := model.SelectionModeLatest

	data := `
configseeder:
  client:
    tenantKey: "` + tenantKey + `"
    environmentKey: "` + environmentKey + `"
    version: "` + version + `"
    context: "` + context + `"
    dateTime: "` + dateTime + `"
    configurations:
    - groupKey: "` + configurationGroup + `"
      configurationGroupId: "` + configurationGroupId + `"
      selectionMode: "` + string(selectionMode) + `"
      versionNumber: "` + configurationVersion + `"
    initializationMode: "` + string(initializationMode) + `"
    refreshMode: "` + string(refreshMode) + `"
    refreshCycle: ` + strconv.Itoa(refreshCycle) + `
    retryWaitTime: ` + strconv.Itoa(retryWaitTime) + `
    maxRetries: ` + strconv.Itoa(maxRetries) + `
    readTimeout: ` + strconv.Itoa(readTimeout) + `
    connectionTimeout: ` + strconv.Itoa(connectionTimeout) + `
    serverUrl: "` + serverUrl + `"
    apiKey: "` + apiKey + `"
`
	root := configurationRoot{}

	if err := yaml.Unmarshal([]byte(data), &root); err != nil {
		t.Fatal(err)
	}

	configuration := root.Configuration
	if configuration == nil {
		t.Fatal("Configuration shouldn't be <nil>")
	}

	client := configuration.ClientConfiguration
	if client == nil {
		t.Error("ClientConfiguration shouldn't be <nil>")
	}

	if client.TenantKey != tenantKey {
		t.Errorf("TenantKey should be '%s' but was '%s'", tenantKey, client.TenantKey)
	}
	if *client.EnvironmentKey != environmentKey {
		t.Errorf("EnvironmentKey should be '%s' but was '%s'", environmentKey, *client.EnvironmentKey)
	}
	if *client.Version != version {
		t.Errorf("Version should be '%s' but was '%s'", version, *client.Version)
	}
	if *client.Context != context {
		t.Errorf("Context should be '%s' but was '%s'", context, *client.Context)
	}
	if client.DateTime.Format(time.RFC3339) != dateTime {
		t.Errorf("DateTime should be '%v' but was '%v'", dateTime, client.DateTime)
	}
	if *client.InitializationMode != initializationMode {
		t.Errorf("InitializationMode should be '%s' but was '%s'", initializationMode, *client.InitializationMode)
	}
	if *client.RefreshMode != refreshMode {
		t.Errorf("RefreshMode should be '%s' but was '%s'", refreshMode, *client.RefreshMode)
	}
	if *client.RefreshCycle != refreshCycle {
		t.Errorf("RefreshCycle should be '%d' but was '%d'", refreshCycle, *client.RefreshCycle)
	}
	if *client.RetryWaitTime != retryWaitTime {
		t.Errorf("RetryWaitTime should be '%d' but was '%d'", retryWaitTime, *client.RetryWaitTime)
	}
	if *client.MaxRetries != maxRetries {
		t.Errorf("MaxRetries should be '%d' but was '%d'", maxRetries, *client.MaxRetries)
	}
	if *client.ReadTimeout != readTimeout {
		t.Errorf("ReadTimeout should be '%d' but was '%d'", readTimeout, *client.ReadTimeout)
	}
	if *client.ConnectionTimeout != connectionTimeout {
		t.Errorf("ConnectionTimeout should be '%d' but was '%d'", connectionTimeout, *client.ConnectionTimeout)
	}
	if *client.ServerUrl != serverUrl {
		t.Errorf("ServerUrl should be '%s' but was '%s'", serverUrl, *client.ServerUrl)
	}
	if *client.ApiKey != apiKey {
		t.Errorf("ApiKey should be '%s' but was '%s'", apiKey, *client.ApiKey)
	}

	// Configurations
	if client.Configurations == nil {
		t.Error("Configurations shouldn't be <nil>")
	}
	if len(client.Configurations) != 1 {
		t.Error("There should be exactly one configuration")
	}

	if client.Configurations[0].GroupKey != configurationGroup {
		t.Errorf("configurations[0].GroupKey should be '%s' but was '%s'", configurationGroup, client.Configurations[0].GroupKey)
	}
	if client.Configurations[0].ConfigurationGroupId != configurationGroupId {
		t.Errorf("configurations[0].ConfigurationGroupId should be '%s' but was '%s'", configurationGroupId, client.Configurations[0].ConfigurationGroupId)
	}
	if *client.Configurations[0].SelectionMode != selectionMode {
		t.Errorf("configurations[0].SelectionMode should be '%s' but was '%s'", selectionMode, *client.Configurations[0].SelectionMode)
	}
	if *client.Configurations[0].Version != configurationVersion {
		t.Errorf("configurations[0].Version should be '%s' but was '%s'", configurationVersion, *client.Configurations[0].Version)
	}
}

func TestIsValidInitializationMode(t *testing.T) {
	if !IsValidInitializationMode(InitializationModeLazy) {
		t.Errorf("%s should be a valid InitializationMode", InitializationModeLazy)
	}
	if !IsValidInitializationMode(InitializationModeEagerAsync) {
		t.Errorf("%s should be a valid InitializationMode", InitializationModeEagerAsync)
	}
	if !IsValidInitializationMode(InitializationModeEager) {
		t.Errorf("%s should be a valid InitializationMode", InitializationModeEager)
	}

	numberOfInitializationModes := 3
	if len(validInitializationModes) != numberOfInitializationModes {
		t.Errorf("There should be exactly %d valid InitializationModes", numberOfInitializationModes)
	}
}

func TestIsValidRefreshMode(t *testing.T) {
	if !IsValidRefreshMode(RefreshModeManual) {
		t.Errorf("%s should be a valid RefreshMode", RefreshModeManual)
	}
	if !IsValidRefreshMode(RefreshModeLazy) {
		t.Errorf("%s should be a valid RefreshMode", RefreshModeLazy)
	}
	if !IsValidRefreshMode(RefreshModeTimer) {
		t.Errorf("%s should be a valid RefreshMode", RefreshModeTimer)
	}
	if !IsValidRefreshMode(RefreshModeNever) {
		t.Errorf("%s should be a valid RefreshMode", RefreshModeNever)
	}

	numberOfRefreshModes := 4
	if len(validRefreshModes) != numberOfRefreshModes {
		t.Errorf("There should be exactly %d valid RefreshModes", numberOfRefreshModes)
	}
}
