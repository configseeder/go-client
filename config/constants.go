package config

const (
	RefreshCycle   int = 15000
	DefaultTimeout int = 500
	ReadTimeout    int = 2000
	MaxRetries     int = 3
	RetryWaitTime  int = 500

	// MaxRetryLowerLimit defines the lower limit for MaxRetry
	MaxRetryLowerLimit int = 1
	// MaxRetryHigherLimit defines the upper limit for MaxRetry
	MaxRetryHigherLimit int = 10
)

const (
	// ConfigseederConfigfile is the filename of the configfile for accessing the configseeder.
	ConfigseederConfigfile = "configseeder.configfile"

	// prefix represents CONFIG_SEEDER_CLIENT_PREFIX. (for environment variables)
	prefix = "configseeder.client."
	// ConfigseederClientServerUrl is the  Config Key for Config Seeder Server URL.
	ConfigseederClientServerUrl = prefix + "serverUrl"
	// ConfigseederClientCertificateFilename is the Config Key for filename for additional certificates.
	ConfigseederClientCertificateFilename = prefix + "certificateFilename"
	// ConfigseederClientApiKey is the Config Key for API Key.
	ConfigseederClientApiKey = prefix + "apiKey"
	// ConfigseederClientTenantKey is the Config Key for Tenant Key.
	ConfigseederClientTenantKey = prefix + "tenantKey"
	// ConfigseederClientContext is the Config Key for Context Filter.
	ConfigseederClientContext = prefix + "context"
	// ConfigseederClientDateTime is the Config Key for Date Time Filter.
	ConfigseederClientDateTime = prefix + "dateTime"
	// ConfigseederClientVersion is the Config Key for Version Filter.
	ConfigseederClientVersion = prefix + "version"
	// ConfigseederClientFullForNonFilteredKey is the Config Key for disabling duplicate elimination.
	ConfigseederClientFullForNonFilteredKey = prefix + "fullForNonFiltered"
	// ConfigseederClientEnvironmentKey is the Config Key for Environment Filter.
	ConfigseederClientEnvironmentKey = prefix + "environmentKey"
	// ConfigseederClientConfigurationGroupKeys is the Config Key for Configuration Filter (simple config).
	ConfigseederClientConfigurationGroupKeys = prefix + "configurationGroupKeys"
	// ConfigseederClientConfigurations is the Config Key for Configuration Filter.
	ConfigseederClientConfigurations = prefix + "configurations"
	// ConfigseederClientConnectionTimeout is the Config Key for Connection Timeout.
	ConfigseederClientConnectionTimeout = prefix + "connectionTimeout"
	// ConfigseederClientReadTimeout is the Config Key for Read Timeout.
	ConfigseederClientReadTimeout = prefix + "readTimeout"
	// ConfigseederClientMaxRetries is the Config Key for Max Retries.
	ConfigseederClientMaxRetries = prefix + "maxRetries"
	// ConfigseederClientRetryWaitTime is the Config Key for Retry Wait Time.
	ConfigseederClientRetryWaitTime = prefix + "retryWaitTime"
	// ConfigseederClientRefreshCycle is the Config Key for refresh cycle.
	ConfigseederClientRefreshCycle = prefix + "refreshCycle"
	// ConfigseederClientRefreshMode is the Config Key for Refresh mode of config seeder client.
	ConfigseederClientRefreshMode = prefix + "refreshMode"
	// ConfigseederClientInitializationMode is the Config Key for Initialization mode of config seeder client.
	ConfigseederClientInitializationMode = prefix + "initializationMode"
	// ConfigseederClientDisable is the Config Key for Disable configseeder client.
	ConfigseederClientDisable = prefix + "disable"
)
