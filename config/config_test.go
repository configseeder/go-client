package config

import (
	"flag"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util_test"
	"os"
	"strings"
	"testing"
	"time"
)

var properties = map[string]string{
	ConfigseederClientDisable:                "true",
	ConfigseederClientApiKey:                 "apiKey",
	ConfigseederClientConfigurationGroupKeys: "group1, group2",
	ConfigseederClientContext:                "context",
	ConfigseederClientConnectionTimeout:      "4",
	ConfigseederClientDateTime:               "2001-02-03T04:05:06Z",
	ConfigseederClientEnvironmentKey:         "environmentKey",
	ConfigseederClientFullForNonFilteredKey:  "true",
	ConfigseederClientInitializationMode:     string(InitializationModeEager),
	ConfigseederClientMaxRetries:             "3",
	ConfigseederClientReadTimeout:            "5",
	ConfigseederClientRefreshCycle:           "1",
	ConfigseederClientRefreshMode:            string(RefreshModeLazy),
	ConfigseederClientRetryWaitTime:          "2",
	ConfigseederClientServerUrl:              "http://serverUrl",
	ConfigseederClientTenantKey:              "tenantKey",
	ConfigseederClientVersion:                "version",
}

/*
 * Testcase for reading yaml file with complete Configurations-Section
 */
func TestGetConfigurationConfiguredByYamlConfigurations(t *testing.T) {

	// Initialize Client
	util_test.ResetFlagsAndEnvironment(properties)
	InitFlags()
	flag.Set(ConfigseederConfigfile, "../testdata/configseeder-configurations.yaml")
	flag.Parse()

	config, err := GetConfiguration()

	if err != nil {
		t.Fatal(err)
	}

	validateClientConfiguration(config, t)
	validateClientConfigConfigurations(config, t)
}

/*
 * Testcase for reading yaml with simplified configurationGroupKeys
 */
func TestGetConfigurationConfiguredByYamlGroupKeys(t *testing.T) {

	// Initialize Client
	util_test.ResetFlagsAndEnvironment(properties)
	InitFlags()
	flag.Set(ConfigseederConfigfile, "../testdata/configseeder-configurationGroupKeys.yaml")
	flag.Parse()

	config, err := GetConfiguration()

	if err != nil {
		t.Fatal(err)
	}

	validateClientConfiguration(config, t)
	validateConfigurationGroupKeys(config, t)
}

/*
 * Testcase for reading yaml file with complete Configurations-Section
 */
func TestGetConfigurationConfiguredByYamlDuplicatedConfigurationGroup(t *testing.T) {

	// Initialize Client
	util_test.ResetFlagsAndEnvironment(properties)
	InitFlags()
	flag.Set(ConfigseederConfigfile, "../testdata/configseeder-duplicatedConfigurationGroup.yaml")
	flag.Parse()

	_, err := GetConfiguration()

	if err == nil {
		t.Fatal("There should be an error with a duplicated configurationGroup")
	}

	if strings.HasSuffix(err.Error(), "is configured more than one time") {
		// expected error
	} else {
		t.Errorf("Unexpected error %s", err)
	}
}

func TestGetConfigurationConfiguredByFlag(t *testing.T) {

	// Initialize Client
	util_test.ResetFlagsAndEnvironment(properties)
	InitFlags()
	for k, v := range properties {
		flag.Set(k, v)
	}

	flag.Parse()

	config, err := GetConfiguration()

	if err != nil {
		t.Fatal(err)
	}

	validateClientConfiguration(config, t)
	validateConfigurationGroupKeys(config, t)
}

func TestGetConfigurationConfiguredByEnv(t *testing.T) {

	// Initialize Client
	util_test.ResetFlagsAndEnvironment(properties)
	InitFlags()
	flag.Parse()

	for k, v := range properties {
		envKey := strings.Replace(k, ".", "_", -1)
		envKey = strings.ToUpper(envKey)
		os.Setenv(envKey, v)
	}

	config, err := GetConfiguration()

	if err != nil {
		t.Fatal(err)
	}

	validateClientConfiguration(config, t)
	validateConfigurationGroupKeys(config, t)
}

func TestValidateContextAndLabelsNothingSet(t *testing.T) {
	// given
	configuration := &ClientConfiguration{
		Context: nil,
		Labels:  nil,
	}

	errorCollection := &model.ErrorCollection{}

	// when
	validateContextAndLabels(configuration, errorCollection)

	// then
	if errorCollection.HasError() {
		t.Errorf("Expected successfull validation but got errors: %s", errorCollection.Error())
	}
}

func TestValidateContextAndLabelsContextSet(t *testing.T) {
	// given
	context := "context"
	configuration := &ClientConfiguration{
		Context: &context,
		Labels:  nil,
	}

	errorCollection := &model.ErrorCollection{}

	// when
	validateContextAndLabels(configuration, errorCollection)

	// then
	if errorCollection.HasError() {
		t.Errorf("Expected successfull validation but got errors: %s", errorCollection.Error())
	}
}

func TestValidateContextAndLabelsLabelSet(t *testing.T) {
	// given
	configuration := &ClientConfiguration{
		Context: nil,
		Labels:  []string{"label"},
	}

	errorCollection := &model.ErrorCollection{}

	// when
	validateContextAndLabels(configuration, errorCollection)

	// then
	if errorCollection.HasError() {
		t.Errorf("Expected successfull validation but got errors: %s", errorCollection.Error())
	}
}

func TestValidateContextAndLabelsBothSet(t *testing.T) {
	// given
	context := "context"
	configuration := &ClientConfiguration{
		Context: &context,
		Labels:  []string{"label"},
	}

	errorCollection := &model.ErrorCollection{}

	// when
	validateContextAndLabels(configuration, errorCollection)

	// then
	if !errorCollection.HasError() {
		t.Error("Expected validation wit error but was successful")
	}
}

func validateClientConfiguration(config *ClientConfiguration, t *testing.T) {
	disable := true
	tenantKey := "tenantKey"
	environmentKey := "environmentKey"
	fullForNonFiltered := true
	version := "version"
	context := "context"
	dateTime := "2001-02-03T04:05:06Z"
	initializationMode := InitializationModeEager
	refreshMode := RefreshModeLazy
	refreshCycle := 1
	retryWaitTime := 2
	maxRetries := 3
	connectionTimeout := 4
	readTimeout := 5
	serverUrl := "http://serverUrl"
	apiKey := "apiKey"

	if config.Disable != disable {
		t.Errorf("Disable should be '%t' but was '%t'", disable, config.Disable)
	}
	if config.TenantKey != tenantKey {
		t.Errorf("TenantKey should be '%s' but was '%s'", tenantKey, config.TenantKey)
	}
	if config.EnvironmentKey == nil {
		t.Errorf("EnvironmentKey should be '%s' but was <nil>", environmentKey)
	} else if *config.EnvironmentKey != environmentKey {
		t.Errorf("EnvironmentKey should be '%s' but was '%s'", environmentKey, *config.EnvironmentKey)
	}
	if config.FullForNonFiltered == nil {
		t.Errorf("FullForNonFilteredKey should be '%t' but was <nil>", fullForNonFiltered)
	} else if *config.FullForNonFiltered != fullForNonFiltered {
		t.Errorf("FullForNonFilteredKey should be '%t' but was '%t'", fullForNonFiltered, *config.FullForNonFiltered)
	}
	if *config.Version != version {
		t.Errorf("Version should be '%s' but was '%s'", version, *config.Version)
	}
	if *config.Context != context {
		t.Errorf("Context should be '%s' but was '%s'", context, *config.Context)
	}
	if config.DateTime.Format(time.RFC3339) != dateTime {
		t.Errorf("DateTime should be '%v' but was '%v'", dateTime, config.DateTime)
	}
	if *config.InitializationMode != initializationMode {
		t.Errorf("InitializationMode should be '%s' but was '%s'", initializationMode, *config.InitializationMode)
	}
	if *config.RefreshMode != refreshMode {
		t.Errorf("RefreshMode should be '%s' but was '%s'", refreshMode, *config.RefreshMode)
	}
	if *config.RefreshCycle != refreshCycle {
		t.Errorf("RefreshCycle should be '%d' but was '%d'", refreshCycle, *config.RefreshCycle)
	}
	if *config.RetryWaitTime != retryWaitTime {
		t.Errorf("RetryWaitTime should be '%d' but was '%d'", retryWaitTime, *config.RetryWaitTime)
	}
	if *config.MaxRetries != maxRetries {
		t.Errorf("MaxRetries should be '%d' but was '%d'", maxRetries, *config.MaxRetries)
	}
	if *config.ReadTimeout != readTimeout {
		t.Errorf("ReadTimeout should be '%d' but was '%d'", readTimeout, *config.ReadTimeout)
	}
	if *config.ConnectionTimeout != connectionTimeout {
		t.Errorf("ConnectionTimeout should be '%d' but was '%d'", connectionTimeout, *config.ConnectionTimeout)
	}
	if *config.ServerUrl != serverUrl {
		t.Errorf("ServerUrl should be '%s' but was '%s'", serverUrl, *config.ServerUrl)
	}
	if *config.ApiKey != apiKey {
		t.Errorf("ApiKey should be '%s' but was '%s'", apiKey, *config.ApiKey)
	}
}

/*
 * Validates Configurations-Object configured with configurationGroupKeys
 */
func validateConfigurationGroupKeys(config *ClientConfiguration, t *testing.T) {
	if config.Configurations == nil {
		t.Fatal("Configurations shouldn't be <nil>")
	}
	if len(config.Configurations) != 2 {
		t.Fatal("There should be exactly two configurations")
	}

	validateConfigurationsEntry(config, 0, "group1", model.SelectionModeLatest, nil, t)
	validateConfigurationsEntry(config, 1, "group2", model.SelectionModeLatest, nil, t)
}

/*
 * Validates detailed Configurations-Object configured by configurations (key, selectionMode, version)
 */
func validateClientConfigConfigurations(config *ClientConfiguration, t *testing.T) {
	if config.Configurations == nil {
		t.Fatal("Configurations shouldn't be <nil>")
	}
	if len(config.Configurations) != 1 {
		t.Fatal("There should be exactly one configuration")
	}

	version := "configurations.version"
	validateConfigurationsEntry(config, 0, "configurations.key", model.SelectionModeVersionNumber, &version, t)
}

func validateConfigurationsEntry(config *ClientConfiguration, index int, key string, selectionMode model.VersionedConfigurationGroupSelectionMode, version *string, t *testing.T) {
	if config.Configurations[index].GroupKey != key {
		t.Errorf("configurations[%d].Key should be '%s' but was '%s'", index, key, config.Configurations[0].GroupKey)
	}
	if *config.Configurations[index].SelectionMode != selectionMode {
		t.Errorf("configurations[%d].SelectionMode should be '%s' but was '%s'", index, selectionMode, *config.Configurations[0].SelectionMode)
	}
	if version == nil && config.Configurations[index].Version != nil {
		t.Errorf("configurations[%d].Version should be <nil> but was '%s'", index, *config.Configurations[0].Version)
	} else if util_test.GetStringValue(config.Configurations[index].Version) != util_test.GetStringValue(version) {
		t.Errorf("configurations[%d].Version should be '%s' but was '%s'", index, util_test.GetStringValue(version), util_test.GetStringValue(config.Configurations[0].Version))
	}
}
