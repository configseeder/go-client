package integration_test

import (
	"encoding/json"
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/configseeder/go-client"
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
	"gitlab.com/configseeder/go-client/util_test"
	"io"
	"net/http"
	"strings"
	"testing"
	"time"
)

var configurationValueId = "configurationValueId"
var context = "context"
var environment = "environment"
var environmentId = "environmentId"
var key = "key"
var lastUpdate = time.Now()
var secured = true
var valueType = model.ValueTypeString
var versionFrom = "versionFrom"
var versionTo = "versionTo"
var validFrom, _ = time.Parse(time.RFC3339, "2018-01-01T08:00:00")
var validTo, _ = time.Parse(time.RFC3339, "2018-12-01T08:00:00")
var value = "value"

/*
 * Test for receiving configuration data with eager mode.
 */
func TestConfigurationGroupsEager(t *testing.T) {
	properties := map[string]string{
		config.ConfigseederClientApiKey:                 "apiKey",
		config.ConfigseederClientConfigurationGroupKeys: "group1",
		config.ConfigseederClientEnvironmentKey:         environment,
		config.ConfigseederClientInitializationMode:     string(config.InitializationModeEager),
		config.ConfigseederClientRefreshMode:            string(config.RefreshModeLazy),
		config.ConfigseederClientServerUrl:              "http://localhost:8000",
		config.ConfigseederClientTenantKey:              "tenantKey",
	}
	util_test.ResetFlagsAndEnvironment(properties)

	var client configseeder.Client = &configseeder.ClientImplementation{}
	client.InitFlags()

	for k, v := range properties {
		_ = flag.Set(k, v)
	}
	flag.Parse()

	// Start Server
	util_test.InitServer(testHandlerIntegrationTest)
	server := util_test.StartServer("8000")
	defer util_test.StopServer(server)

	// Initialize Client
	client.ResetInitialization()
	_ = client.Init(nil)

	// Get Configuration
	configValues, err := client.GetConfigValues()
	if err != nil {
		t.Fatalf("Unable to get ConfigurationValues from Simulator: %s", err)
	}

	// Compare Values
	if len(configValues) != 1 {
		t.Fatalf("ConfigurationValues should contain exactly one value, was %d", len(configValues))
	}

	configValue := configValues[0]

	if configValue.ConfigurationValueId != configurationValueId {
		t.Fatalf("ConfigurationValueId should be '%s' but was %s", configurationValueId, configValue.ConfigurationValueId)
	}
	if util_test.GetStringValue(configValue.Context) != context {
		t.Fatalf("Context should be '%s' but was %s", context, util_test.GetStringValue(configValue.Context))
	}
	if util_test.GetStringValue(configValue.Environment) != environment {
		t.Fatalf("Environment should be '%s' but was %s", environment, util_test.GetStringValue(configValue.Environment))
	}
	if util_test.GetStringValue(configValue.EnvironmentId) != environmentId {
		t.Fatalf("EnvironmentId should be '%s' but was %s", environmentId, util_test.GetStringValue(configValue.EnvironmentId))
	}
	if configValue.Key != key {
		t.Fatalf("Key should be '%s' but was %s", key, configValue.Key)
	}
	if configValue.LastUpdateInMilliseconds != util.TimeInMilliseconds(lastUpdate) {
		t.Fatalf("LastUpdate should be '%d' but was %d", util.TimeInMilliseconds(lastUpdate), configValue.LastUpdateInMilliseconds)
	}
	if configValue.LastUpdate != lastUpdate.String() {
		t.Fatalf("LastUpdateAsString should be '%s' but was %s", lastUpdate.String(), configValue.LastUpdate)
	}
	if configValue.Secured != secured {
		t.Fatalf("Secured should be '%t' but was %t", secured, configValue.Secured)
	}
	if configValue.Type != valueType {
		t.Fatalf("Type should be '%s' but was %s", valueType, configValue.Type)
	}
	if *configValue.ValidFrom != validFrom {
		t.Fatalf("ValidFrom should be '%v' but was %v", validFrom, configValue.ValidFrom)
	}
	if *configValue.ValidTo != validTo {
		t.Fatalf("ValidTo should be '%v' but was %v", validTo, configValue.ValidTo)
	}
	if configValue.Value != value {
		t.Fatalf("Value should be '%s' but was %s", value, configValue.Value)
	}
	if util_test.GetStringValue(configValue.VersionFrom) != versionFrom {
		t.Fatalf("VersionFrom should be '%s' but was %s", versionFrom, util_test.GetStringValue(configValue.VersionFrom))
	}
	if util_test.GetStringValue(configValue.VersionTo) != versionTo {
		t.Fatalf("VersionTo should be '%s' but was %s", versionTo, util_test.GetStringValue(configValue.VersionTo))
	}
}

/*
 * Tests if configured Listener is called when configuration data changes.
 */
func TestConfigurationGroupsTimer(t *testing.T) {
	properties := map[string]string{
		config.ConfigseederClientApiKey:                 "apiKey",
		config.ConfigseederClientConfigurationGroupKeys: "timer",
		config.ConfigseederClientEnvironmentKey:         environment,
		config.ConfigseederClientInitializationMode:     string(config.InitializationModeEager),
		config.ConfigseederClientRefreshMode:            string(config.RefreshModeTimer),
		config.ConfigseederClientRefreshCycle:           "2000",
		config.ConfigseederClientServerUrl:              "http://localhost:8001",
		config.ConfigseederClientTenantKey:              "tenantKey",
	}
	util_test.ResetFlagsAndEnvironment(properties)
	client := configseeder.ClientImplementation{}
	client.InitFlags()

	for k, v := range properties {
		_ = flag.Set(k, v)
	}
	flag.Parse()

	// Start Server
	util_test.InitServer(testHandlerIntegrationTest)
	server := util_test.StartServer("8001")
	defer util_test.StopServer(server)

	// Initialize Client
	client.ResetInitialization()
	err := client.Init(nil)
	if err != nil {
		t.Fatalf(err.Error())
	}
	time.Sleep(time.Second * 2)
	client.Subscribe(TimerTestListener)

	time.Sleep(time.Duration(time.Second * 2))

	if !keyReceived {
		t.Fatalf("Listener should have received update for key %s", key)
	}
}

var keyReceived = false

func TimerTestListener(configValueChanges []model.ConfigValueChange) {
	log.Info("TimerTestListener called")
	if len(configValueChanges) == 1 {
		configValue := configValueChanges[0]
		if configValue.NewValue.Key == key {
			keyReceived = true
		}
	}
}

func testHandlerIntegrationTest(writer http.ResponseWriter, request *http.Request) {
	if ok := authorizationOk(writer, request); !ok {
		return
	}

	defer request.Body.Close()

	if request.Method == "POST" && request.RequestURI == "/public/api/v1/configurations" {
		handleGetConfigurationRequest(writer, request)
	} else if request.Method == "GET" && request.RequestURI == "/.well-known/config-seeder/client/global-config" {
		handleGetWellKnownRequest(writer, request)
	} else {
		log.Warnf("Invalid %s call to %s", request.Method, request.URL.Path)
	}
}

func handleGetConfigurationRequest(httpWriter http.ResponseWriter, httpRequest *http.Request) {
	if ok := acceptOk(httpWriter, httpRequest, "application/metadata+json"); !ok {
		return
	}

	requestBody, err := io.ReadAll(httpRequest.Body)

	if err != nil {
		log.Warnf("Unable to read body from request: %s", err)
	}

	var request model.ConfigurationRequestDto
	_ = json.Unmarshal(requestBody, &request)

	var response []model.ConfigValueDto

	if len(request.Configurations) == 0 {
		log.Infof("Got request without Configurations, return empty list")
		response = []model.ConfigValueDto{}
	} else {
		lastUpdateToUse := lastUpdate
		if request.Configurations[0].ConfigurationGroupKey != nil && *request.Configurations[0].ConfigurationGroupKey == "timer" {
			lastUpdateToUse = time.Now()
		}

		response = []model.ConfigValueDto{
			{
				Key:                      key,
				Value:                    value,
				Type:                     valueType,
				Context:                  &context,
				Secured:                  secured,
				ConfigurationValueId:     configurationValueId,
				Environment:              &environment,
				EnvironmentId:            &environmentId,
				ValidTo:                  &validTo,
				ValidFrom:                &validFrom,
				VersionFrom:              &versionFrom,
				VersionTo:                &versionTo,
				LastUpdateInMilliseconds: util.TimeInMilliseconds(lastUpdateToUse),
				LastUpdate:               lastUpdateToUse.String(),
			},
		}
	}

	if response != nil {
		responseString, err := json.Marshal(response)
		if err != nil {
			log.Warnf("Unable serialise response to json: %s", err)
		} else {
			_, _ = fmt.Fprintf(httpWriter, string(responseString))
		}
	}
}

func handleGetWellKnownRequest(httpWriter http.ResponseWriter, request *http.Request) {
	if ok := acceptOk(httpWriter, request, "application/json"); !ok {
		return
	}

	responseJson := `{
	"urls":{
		"logo":"/assets/img/ConfigSeeder.svg",
		"baseUrl":"https://staging-postgresql-config-seeder.oneo.cloud",
		"restInternal":"/internal","restPublic":"/public/api/v1",
		"restExternal":null
	},
	"release":{
		"version":"2.25.0",
		"timestamp":"2022-02-02T04:47:59+0000",
		"branchName":"staging",
		"buildNumber":"bfad54e"
	}
}`
	_, err := fmt.Fprintf(httpWriter, responseJson)
	if err != nil {
		log.Warnf("Unable to return well known information: %s", err)
	}
}

func acceptOk(writer http.ResponseWriter, request *http.Request, expectedHeader string) bool {
	header := request.Header.Get("Accept")
	if len(header) == 0 {
		writer.WriteHeader(http.StatusBadRequest)
		_, _ = writer.Write([]byte("400 - No Accept header found"))
		return false
	}
	if header != expectedHeader {
		writer.WriteHeader(http.StatusBadRequest)
		_, _ = writer.Write([]byte(fmt.Sprintf("400 - Accept Header should be %s but was %s", expectedHeader, header)))
		return false
	}

	return true
}
func authorizationOk(writer http.ResponseWriter, request *http.Request) bool {
	authorizationHeader := request.Header.Get("AUTHORIZATION")
	if len(authorizationHeader) == 0 {
		writer.WriteHeader(http.StatusUnauthorized)
		_, _ = writer.Write([]byte("401 - No AUTHORIZATION header found"))
		return false
	}
	if !strings.HasPrefix(strings.ToUpper(authorizationHeader), "BEARER ") {
		writer.WriteHeader(http.StatusUnauthorized)
		_, _ = writer.Write([]byte("401 - Autorization only allowed with Bearer Token"))
		return false
	}

	return true
}
