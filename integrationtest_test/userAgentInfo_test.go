package integration_test

import (
	"encoding/json"
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/configseeder/go-client"
	"gitlab.com/configseeder/go-client/config"
	"gitlab.com/configseeder/go-client/model"
	"gitlab.com/configseeder/go-client/util"
	"gitlab.com/configseeder/go-client/util_test"
	"net/http"
	"os"
	"testing"
)

/*
 * Test for receiving configuration data with eager mode.
 */
func TestCustomHeaders(t *testing.T) {
	properties := map[string]string{
		config.ConfigseederClientApiKey:                 "apiKey",
		config.ConfigseederClientConfigurationGroupKeys: "group1",
		config.ConfigseederClientEnvironmentKey:         environment,
		config.ConfigseederClientInitializationMode:     string(config.InitializationModeEager),
		config.ConfigseederClientRefreshMode:            string(config.RefreshModeLazy),
		config.ConfigseederClientServerUrl:              "http://localhost:8002",
		config.ConfigseederClientTenantKey:              "tenantKey",
	}
	util_test.ResetFlagsAndEnvironment(properties)

	var client configseeder.Client = &configseeder.ClientImplementation{}
	client.InitFlags()

	for k, v := range properties {
		_ = flag.Set(k, v)
	}
	flag.Parse()

	serverObject := &ServerObject{}

	// Start Server
	util_test.InitServer(serverObject.testHandlerUserAgentTest)
	server := util_test.StartServer("8002")
	defer util_test.StopServer(server)

	// Initialize Client
	client.ResetInitialization()
	_ = client.Init(nil)

	// Get Configuration
	_, err := client.GetConfigValues()
	if err != nil {
		t.Fatalf("Unable to get ConfigurationValues from Simulator: %s", err)
	}

	if len(serverObject.identification) == 0 {
		t.Error("Header X-Host-Identification should have been set")
	}
	expectedHostname, _ := os.Hostname()
	if len(serverObject.hostname) == 0 {
		t.Error("Header X-Hostname should have been set")
	} else if expectedHostname != serverObject.hostname {
		t.Errorf("Expected Header X-Hostname to have value %s but was %s", expectedHostname, serverObject.hostname)
	}
	expectedUserAgent := "ConfigSeederClient-Go/" + util.GoClientVersion + " (linux)"
	if len(serverObject.userAgent) == 0 {
		t.Error("Header X-User-Agent")
	} else if expectedUserAgent != serverObject.userAgent {
		t.Errorf("Expected Header X-User-Agent to have value %s but was %s", expectedUserAgent, serverObject.userAgent)
	}
}

type ServerObject struct {
	identification string
	hostname       string
	userAgent      string
}

func (s *ServerObject) testHandlerUserAgentTest(writer http.ResponseWriter, request *http.Request) {
	if ok := authorizationOk(writer, request); !ok {
		return
	}

	defer request.Body.Close()

	if request.Method == "POST" && request.RequestURI == "/public/api/v1/configurations" {
		s.testUserAgentHandler(writer, request)
	} else if request.Method == "GET" && request.RequestURI == "/.well-known/config-seeder/client/global-config" {
		handleGetWellKnownRequest(writer, request)
	} else {
		log.Warnf("Invalid %s call to %s", request.Method, request.URL.Path)
	}
}

func (s *ServerObject) testUserAgentHandler(writer http.ResponseWriter, request *http.Request) {

	s.identification = request.Header.Get("X-Host-Identification")
	s.hostname = request.Header.Get("X-Hostname")
	s.userAgent = request.Header.Get("X-User-Agent")

	response := []model.ConfigValueDto{}
	responseString, err := json.Marshal(response)
	if err != nil {
		log.Warnf("Unable serialise response to json: %s", err)
	} else {
		_, _ = fmt.Fprintf(writer, string(responseString))
	}
}
