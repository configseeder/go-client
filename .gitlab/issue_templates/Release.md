## Release go-client [version]

### Build

- [ ] Verify and update Changelog
- [ ] Verify functionality with go-demo
- [ ] Create Release Build

### Make release available

- Update Download descriptor
  - [ ] Test on staging
  - [ ] Merge to master

### Merge to master  
  
- [ ] Merge Release from staging to master