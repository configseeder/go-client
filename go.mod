module gitlab.com/configseeder/go-client

go 1.22

require (
	github.com/Masterminds/semver/v3 v3.3.1
	github.com/sirupsen/logrus v1.9.3
	gopkg.in/yaml.v2 v2.4.0
)

require golang.org/x/sys v0.28.0 // indirect
